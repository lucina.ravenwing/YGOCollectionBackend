namespace YGOCollectionBackend.HttpWrapper; 

/// <summary>
/// Interface used to allow for HttpClient to be mocked and allow the rest of the code to be tested in isolation.
/// </summary>
public interface IHttpClient {
    /// <summary>
    /// Method that calls PostAsync in the HttpClient class. Interfaced to allow for testing.
    /// </summary>
    /// <param name="url">The URL to call into.</param>
    /// <param name="content">The body of the HTTP request.</param>
    /// <returns>The response of the Post call.</returns>
    Task<HttpResponseMessage> PostAsync(string url, StringContent content);
}