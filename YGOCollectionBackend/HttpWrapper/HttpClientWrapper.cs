using System.Net.Http.Headers;

namespace YGOCollectionBackend.HttpWrapper; 

/// <summary>
/// Wrapper class that maintains the calls the the HttpClient to allow for testability.
/// </summary>
public class HttpClientWrapper : IHttpClient {

    private readonly HttpClient _client;

    /// <summary>
    /// Dependency injected constructor for HttpClientWrapper class. Sets the data it accepts to be json.
    /// </summary>
    /// <param name="client">The actual HttpClient object that makes HTTP calls.</param>
    public HttpClientWrapper(HttpClient client) {
        _client = client;
        
        _client.DefaultRequestHeaders
            .Accept
            .Add(new MediaTypeWithQualityHeaderValue("application/json"));
    }
    
    /// <summary>
    /// Run PostAsync method from HttpClient.
    /// </summary>
    /// <param name="url">The URL to call to.</param>
    /// <param name="content">The body of the HTTP call.</param>
    /// <returns>The response of the Post call.</returns>
    public async Task<HttpResponseMessage> PostAsync(string url, StringContent content) {
        return await _client.PostAsync(url, content);
    }
}