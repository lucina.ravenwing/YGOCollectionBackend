using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Repos.Interfaces; 

/// <summary>
/// Interface for decks. Hides the full functionality of Entity Framework and allows for easier testing.
/// </summary>
public interface IDeckRepo {


    /// <summary>
    /// Gets a user's decks by their id.
    /// </summary>
    /// <param name="userId">User ID to search in table.</param>
    /// <returns>Collection of decks owned by the user with given ID.</returns>
    IEnumerable<Deck> GetDecksByUserId(long userId);

    /// <summary>
    /// Gets a deck by the deck id.
    /// </summary>
    /// <param name="deckId">The ID of the deck to pull.</param>
    /// <returns>The deck that was found or a new Deck if none were in the database.</returns>
    Task<Deck> GetDeckByDeckIdAsync(long deckId);

    /// <summary>
    /// Checks whether the given deck exists for the given user.
    /// </summary>
    /// <param name="userId">The id of the user.</param>
    /// <param name="deckName">The deck name to search.</param>
    /// <returns>Boolean denoting if there is a deck that exists.</returns>
    bool ExistsByUserIdAndName(long userId, string deckName);
    
    /// <summary>
    /// Saves a given deck to the database.
    /// </summary>
    /// <param name="deck">The deck to be saved.</param>
    /// <returns>The deck object that was saved.</returns>
    Task<Deck> SaveDeckAsync(Deck deck);

    /// <summary>
    /// Updates a given deck to the database.
    /// </summary>
    /// <param name="deck">The deck to be updated.</param>
    /// <returns>The deck object that was updated.</returns>
    Task<Deck> UpdateDeck(Deck deck);
    
    /// <summary>
    /// Deletes a deck from the database by the deck ID.
    /// </summary>
    /// <param name="deckId">The id of the deck to be deleted.</param>
    /// <returns>A boolean with the result of the delete operation.</returns>
    Task<bool> DeleteDeckById(long deckId);

}