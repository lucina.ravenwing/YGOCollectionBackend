using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Repos.Interfaces; 

/// <summary>
/// Interface for cards. Hides full functionality of entity framework and allows for easier testing.
/// </summary>
public interface ICardRepo {

    /// <summary>
    /// Gets the card with the given id from the database. Returns a blank card if not found.
    /// </summary>
    /// <param name="id">ID of the card to search.</param>
    /// <returns>Card with given ID.</returns>
    Task<Card> GetCardByIdAsync(long id);

    /// <summary>
    /// Gets the cards with the given name.
    /// </summary>
    /// <param name="name">Name of the card to search.</param>
    /// <returns>Card with given name.</returns>
    Task<Card> GetCardByNameAsync(string name);

    /// <summary>
    /// Placeholder for the start of more complex EF lookups. Note for developer: https://gunnarpeipman.com/ef-core-like-operator/
    /// </summary>
    /// <param name="snippet">Text snippet to search for in Description.</param>
    /// <returns>List of cards containing given snippet.</returns>
    IQueryable<Card> GetCardByDescriptionLike(string snippet);

}