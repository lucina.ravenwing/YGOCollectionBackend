using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Repos.Interfaces; 

/// <summary>
/// Interface for card sets. Hides full functionality of entity framework and allows for easier testing.
/// </summary>
public interface ICardSetRepo {

    /// <summary>
    /// Gets a card set by the card set code.
    /// </summary>
    /// <param name="cardSetCode">The code of the card to search.</param>
    /// <returns>Collection of card set entities matching the cardSetCode passed in.</returns>
    IQueryable<CardSet> GetCardSetsByCode(string cardSetCode);

    /// <summary>
    /// Gets the card set by the card set code and rarity code.
    /// </summary>
    /// <param name="cardSetCode">The code of the card to search.</param>
    /// <param name="cardSetRarityCode">The rarity code of the card set to search.</param>
    /// <returns>Single card set entity matching the cardSetCode and cardSetRarityCode passed in.</returns>
    Task<CardSet> GetCardSetByCodeAndRarityCodeAsync(string cardSetCode, string cardSetRarityCode);

    /// <summary>
    /// Gets the card sets by the set.
    /// </summary>
    /// <param name="setName">The set to search.</param>
    /// <returns>Collection of card set objects in the given set.</returns>
    IQueryable<CardSet> GetCardSetsBySet(string setName);

    /// <summary>
    /// Gets the cards sets associated with the given card ID.
    /// </summary>
    /// <param name="cardId">The card ID to search.</param>
    /// <returns>Collection of card set objects with the given card ID.</returns>
    IQueryable<CardSet> GetCardSetsByCardId(int cardId);

}