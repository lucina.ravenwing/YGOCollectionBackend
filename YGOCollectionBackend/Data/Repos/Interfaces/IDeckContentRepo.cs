using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Repos.Interfaces; 

/// <summary>
/// Interface for deck contents. Hides full functionality of entity framework and allows for easier testing.
/// </summary>
public interface IDeckContentRepo {

    /// <summary>
    /// Gets a deck's content by the deck's primary key.
    /// </summary>
    /// <param name="deckId">The id of the deck to search.</param>
    /// <returns>Collection of deck contents in the deck to search.</returns>
    IEnumerable<DeckContent> GetDeckContentsByDeckId(long deckId);

    /// <summary>
    /// Saves deck contents to the database.
    /// </summary>
    /// <param name="deckContents">An enumerable of the deck contents to be saved.</param>
    /// <returns>A task for the await.</returns>
    Task SaveDeckContentAsync(IEnumerable<DeckContent> deckContents);

    /// <summary>
    /// Deletes all deck contents associated with a given deck ID.
    /// </summary>
    /// <param name="deckId">The ID of the deck to be deleted.</param>
    /// <returns>A task for the await.</returns>
    Task DeleteDeckContentsByDeckIdAsync(long deckId);

}