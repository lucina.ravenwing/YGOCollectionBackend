using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Repos.Interfaces; 

/// <summary>
/// Interface for collections. Hides full functionality of entity framework and allows for easier testing.
/// </summary>
public interface ICollectionRepo {

    /// <summary>
    /// Gets the user's collection by their id.
    /// </summary>
    /// <param name="userId">ID of the collection owner to search.</param>
    /// <returns>Collection of collections with the user ID to search.</returns>
    IQueryable<Collection> GetCollectionsByUserId(long userId);

}