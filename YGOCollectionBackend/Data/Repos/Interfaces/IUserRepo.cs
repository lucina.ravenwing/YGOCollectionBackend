using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Repos.Interfaces; 

/// <summary>
/// Interface for users. Hides the full functionality of Entity Framework and allows for easier testing.
/// </summary>
public interface IUserRepo {

    /// <summary>
    /// Gets a user by their id.
    /// </summary>
    /// <param name="id">ID of the user to search.</param>
    /// <returns>The user with the given ID.</returns>
    Task<User> GetUserByIdAsync(long id);

    /// <summary>
    /// Gets a user by their username.
    /// </summary>
    /// <param name="username">Username of the user to search.</param>
    /// <returns>The user with the given username.</returns>
    Task<User> GetUserByUsernameAsync(string username);
    
    /// <summary>
    /// Gets a user by their email.
    /// </summary>
    /// <param name="email">Email of the user to search.</param>
    /// <returns>The user with the given email.</returns>
    Task<User> GetUserByEmailAsync(string email);

    /// <summary>
    /// Saves a user to the database.
    /// </summary>
    /// <param name="user">The user to be added to the database.</param>
    /// <returns>EntityEntry with a User entity saying if the data was entered.</returns>
    Task<User> CreateUserAsync(User user);

}