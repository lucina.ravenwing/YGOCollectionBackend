using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Repos.Interfaces; 

/// <summary>
/// Interface for wishlists. Hides the full functionality of Entity Framework and allows for easier testing.
/// </summary>
public interface IWishlistRepo {

    /// <summary>
    /// Gets a user's wishlist by their id.
    /// </summary>
    /// <param name="userId">ID of the user's wishlist to search.</param>
    /// <returns>The user's wishlist.</returns>
    IQueryable<Wishlist> GetWishlistsByUserId(long userId);

}