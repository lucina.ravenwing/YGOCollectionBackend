using Microsoft.EntityFrameworkCore;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;

namespace YGOCollectionBackend.Data.Repos.Implementations; 
/// <summary>
/// Implementation of the deck content repo interface. Used to control what calls can be made into the database.
/// </summary>
/// <remarks>See also: <see cref="DeckContent"/></remarks>
public class DeckContentRepoImpl : IDeckContentRepo {

    private readonly YgoDbContext _context;

    /// <summary>
    /// All args constructor for the DeckContentsRepoImpl class.
    /// </summary>
    /// <param name="context">The database context for the project.</param>
    public DeckContentRepoImpl(YgoDbContext context) {
        _context = context;
    }
    
    /// <summary>
    /// Gets the contents of a deck based off of the deck's primary key.
    /// </summary>
    /// <param name="deckId">The ID of the deck to search.</param>
    /// <returns>A collection of the deck contents associated with the deck ID.</returns>
    public IEnumerable<DeckContent> GetDeckContentsByDeckId(long deckId) {
        return _context.DeckContents.Include("CardSet").Include("CardSet.Card").Where(deckContentsQuery => deckContentsQuery.DeckId == deckId);
    }

    /// <summary>
    /// Saves deck contents to the database.
    /// </summary>
    /// <param name="deckContents">An enumerable of the deck contents to be saved.</param>
    /// <returns>A task to await.</returns>
    public async Task SaveDeckContentAsync(IEnumerable<DeckContent> deckContents) {
        await _context.DeckContents.AddRangeAsync(deckContents);
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Deletes all deck contents associated with a given deck ID.
    /// </summary>
    /// <param name="deckId">The ID of the deck to be deleted.</param>
    /// <returns>A task for to await</returns>
    public async Task DeleteDeckContentsByDeckIdAsync(long deckId) {
        _context.DeckContents.RemoveRange(_context.DeckContents.Where(query => query.DeckId == deckId));
        await _context.SaveChangesAsync();
    }
}