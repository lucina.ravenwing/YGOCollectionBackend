using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;

namespace YGOCollectionBackend.Data.Repos.Implementations; 

/// <summary>
/// Implementation of the wishlist repo interface. Used to control what calls can be made into the database.
/// </summary>
/// <remarks>See also: <see cref="Wishlist"/></remarks>
public class WishlistRepoImpl : IWishlistRepo {

    private readonly YgoDbContext _context;

    /// <summary>
    /// All args constructor for the WishlistRepoImpl class.
    /// </summary>
    /// <param name="context">The database context for the project.</param>
    public WishlistRepoImpl(YgoDbContext context) {
        _context = context;
    }
    
    /// <summary>
    /// Gets a user's wishlist by their id.
    /// </summary>
    /// <param name="userId">The ID of the user whose wishlist is being searched.</param>
    /// <returns>A collection with all wishlisted items associated with the given user ID.</returns>
    public IQueryable<Wishlist> GetWishlistsByUserId(long userId) {
        return _context.Wishlists.Where(wishlistQuery => wishlistQuery.UserId == userId);
    }
}