using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;

namespace YGOCollectionBackend.Data.Repos.Implementations; 

/// <summary>
/// Implementation of Collection Repo interface. Used to control what calls can be made into the database.
/// </summary>
/// <remarks>See also: <see cref="Collection"/></remarks>
public class CollectionRepoImpl : ICollectionRepo {

    private readonly YgoDbContext _context;

    /// <summary>
    /// All args constructor for CollectionRepoImpl.
    /// </summary>
    /// <param name="context">The database context for the project.</param>
    public CollectionRepoImpl(YgoDbContext context) {
        _context = context;
    }
    
    /// <summary>
    /// Gets a user's collection based off of their user id.
    /// </summary>
    /// <param name="userId">The user ID of the owner of the Collection.</param>
    /// <returns>A Collection containing the collection owned by the user with the given ID.</returns>
    public IQueryable<Collection> GetCollectionsByUserId(long userId) {
        return _context.Collections.Where(collectionQuery => collectionQuery.UserId == userId);
    }
}