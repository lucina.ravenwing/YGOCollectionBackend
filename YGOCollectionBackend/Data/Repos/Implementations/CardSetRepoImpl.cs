using Microsoft.EntityFrameworkCore;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;

namespace YGOCollectionBackend.Data.Repos.Implementations; 

/// <summary>
/// Implementation of Card Set Repo interface. used to control what calls can be made into the database.
/// </summary>
/// <remarks>See also: <see cref="CardSet"/></remarks>
public class CardSetRepoImpl : ICardSetRepo {

    private readonly YgoDbContext _context;

    /// <summary>
    /// All args constructor for CardSetRepoImpl.
    /// </summary>
    /// <param name="context">The database context for the project.</param>
    public CardSetRepoImpl(YgoDbContext context) {
        _context = context;
    }
    
    /// <summary>
    /// Gets card sets by the card set code.
    /// </summary>
    /// <param name="cardSetCode">The card set code to search on.</param>
    /// <returns>A collection with the returned rows with the given card set code.</returns>
    public IQueryable<CardSet> GetCardSetsByCode(string cardSetCode) {
        return _context.CardSets.Where(cardSetQuery => cardSetQuery.CardSetCode == cardSetCode);
    }
    
    /// <summary>
    /// IGets card set by the card set code and the rarity code.
    /// </summary>
    /// <param name="cardSetCode">The card set code to search on.</param>
    /// <param name="cardSetRarityCode">The card set rarity code to search on.</param>
    /// <returns>The card that matches both the card set code and card set rarity code.</returns>
    public async Task<CardSet> GetCardSetByCodeAndRarityCodeAsync(string cardSetCode, string cardSetRarityCode) {
        return await _context.CardSets.FirstOrDefaultAsync(cardSetQuery =>
            cardSetQuery.CardSetCode == cardSetCode && cardSetQuery.CardSetRarityCode == cardSetRarityCode) 
               ?? new CardSet();
    }

    /// <summary>
    /// Gets all card sets by the set.
    /// </summary>
    /// <param name="setName">The name of the set to search.</param>
    /// <returns>A collection with the cards from the given set.</returns>
    public IQueryable<CardSet> GetCardSetsBySet(string setName) {
        return _context.CardSets.Where(cardSetQuery => cardSetQuery.SetName == setName);
    }

    /// <summary>
    /// Gets the cards sets associated with the given card ID.
    /// </summary>
    /// <param name="cardId">The card ID to search.</param>
    /// <returns>Collection of card set objects with the given card ID.</returns>
    public IQueryable<CardSet> GetCardSetsByCardId(int cardId) {
        return _context.CardSets.Where(cardSetQuery => cardSetQuery.CardId == cardId);
    }
}