using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;

namespace YGOCollectionBackend.Data.Repos.Implementations;

/// <summary>
/// Implementation of the deck repo interface. Used to control what calls can be made into the database.
/// </summary>
/// <remarks>See also: <see cref="Deck"/></remarks>
public class DeckRepoImpl : IDeckRepo {
    private readonly YgoDbContext _context;

    /// <summary>
    /// Dependency injected all args constructor.
    /// </summary>
    /// <param name="context">The database context used to call into the Deck table.</param>
    public DeckRepoImpl(YgoDbContext context) {
        _context = context;
    }

    /// <summary>
    /// Gets a user's decks based on their ID.
    /// </summary>
    /// <param name="userId">User ID to search for owned decks.</param>
    /// <returns>Collection of decks associated with the user ID.</returns>
    public IEnumerable<Deck> GetDecksByUserId(long userId) {
        return _context.Decks.Where(deckQuery => deckQuery.UserId == userId);
    }

    /// <summary>
    /// Gets a deck by the deck id.
    /// </summary>
    /// <param name="deckId">The ID of the deck to pull.</param>
    /// <returns>The deck that was found or a new Deck if none were in the database.</returns>
    public async Task<Deck> GetDeckByDeckIdAsync(long deckId) {
        Deck? deck = await _context.Decks.FindAsync(deckId);

        return deck ?? new Deck();
    }

    /// <summary>
    /// Saves a given deck to the database.
    /// </summary>
    /// <param name="deck">The deck to be saved.</param>
    /// <returns>The deck object that was saved.</returns>
    public async Task<Deck> SaveDeckAsync(Deck deck) {
        EntityEntry<Deck> response = await _context.Decks.AddAsync(deck);

        if (response.State == EntityState.Added) {
            await _context.SaveChangesAsync();

            return response.Entity;
        }

        return new Deck();
    }

    /// <summary>
    /// Updates a given deck to the database.
    /// </summary>
    /// <param name="deck">The deck to be updated.</param>
    /// <returns>The deck object that was updated.</returns>
    public async Task<Deck> UpdateDeck(Deck deck) {
        EntityEntry<Deck> response = _context.Decks.Update(deck);

        if (response.State == EntityState.Modified) {
            await _context.SaveChangesAsync();

            return response.Entity;
        }

        return new Deck();
    }

    /// <summary>
    /// Deletes a deck from the database by the deck ID.
    /// </summary>
    /// <param name="deckId">The id of the deck to be deleted.</param>
    /// <returns>A boolean with the result of the delete operation.</returns>
    public async Task<bool> DeleteDeckById(long deckId) {
        Deck deck = await _context.Decks.FirstAsync(deck => deck.Id == deckId);

        EntityEntry<Deck> response =
            _context.Decks.Remove(deck);

        if (response.State == EntityState.Deleted) {
            await _context.SaveChangesAsync();

            return true;
        }

        return false;
    }

    /// <summary>
    /// Checks whether the given deck exists for the given user.
    /// </summary>
    /// <param name="userId">The id of the user.</param>
    /// <param name="deckName">The deck name to search.</param>
    /// <returns>Boolean denoting if there is a deck that exists.</returns>
    public bool ExistsByUserIdAndName(long userId, string deckName) {
        List<Deck> decks = _context.Decks.Where(deck => deck.DeckName == deckName && deck.UserId == userId).ToList();

        return decks.Count > 0;
    }
}