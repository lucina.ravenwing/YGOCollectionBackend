using Microsoft.EntityFrameworkCore;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;

namespace YGOCollectionBackend.Data.Repos.Implementations; 

/// <summary>
/// Implementation of Card Repo interface. Used to control what calls can be made into the database.
/// </summary>
/// <remarks>See also: <see cref="Card"/></remarks>
public class CardRepoImpl : ICardRepo {

    private readonly YgoDbContext _context;

    /// <summary>
    /// All args constructor for CardRepoImpl.
    /// </summary>
    /// <param name="context">The database context for the project.</param>
    public CardRepoImpl(YgoDbContext context) {
        _context = context;
    }
    
    /// <summary>
    /// Gets the card with the given id from the database. Returns a blank card if not found.
    /// </summary>
    /// <param name="id">ID of the card to search.</param>
    /// <returns>The card with the given ID if found or a new card if not.</returns>
    public async Task<Card> GetCardByIdAsync(long id) {
        return await _context.Cards.FirstOrDefaultAsync(cardQuery => cardQuery.Id == id) ?? new Card();
    }

    /// <summary>
    /// Gets the cards with the given name.
    /// </summary>
    /// <param name="name">Name of the card to search.</param>
    /// <returns>The card with the given name if found or a new card if not.</returns>
    public async Task<Card> GetCardByNameAsync(string name) {
        return await _context.Cards.FirstOrDefaultAsync(cardQuery => cardQuery.Name == name) ?? new Card();
    }

    /// <summary>
    /// Gets cards based off a search string being contained in a description/effect.
    /// </summary>
    /// <param name="snippet"></param>
    /// <returns></returns>
    public IQueryable<Card> GetCardByDescriptionLike(string snippet) {
        return _context.Cards.Where(cardQuery => EF.Functions.Like(cardQuery.CardText ?? string.Empty, "%" + snippet + "%"));
    }
}