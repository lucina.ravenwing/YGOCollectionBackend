using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;

namespace YGOCollectionBackend.Data.Repos.Implementations; 

/// <summary>
/// Implementation of the user repo interface. Used to control what calls can be made into the database.
/// </summary>
/// <remarks>See also: <see cref="User"/></remarks>
public class UserRepoImpl : IUserRepo {

    private readonly YgoDbContext _context;

    /// <summary>
    /// All args constructor for the UserRepoImpl class.
    /// </summary>
    /// <param name="context">The database context for the project.</param>
    public UserRepoImpl(YgoDbContext context) {
        _context = context;
    }
    
    /// <summary>
    /// Gets a user by their id.
    /// </summary>
    /// <param name="id">ID of the user to search.</param>
    /// <returns>The user associated with the ID or a blank user if none are found.</returns>
    public async Task<User> GetUserByIdAsync(long id) {
        return await _context.Users.FirstOrDefaultAsync(userQuery => userQuery.Id == id) ?? new User();
    }

    /// <summary>
    /// Gets a user by their username.
    /// </summary>
    /// <param name="username">Username of the user to search.</param>
    /// <returns>The user associated with the username or a blank user if none are found.</returns>
    public async Task<User> GetUserByUsernameAsync(string username) {
        return await _context.Users.FirstOrDefaultAsync(userQuery => userQuery.Username == username) ?? new User();
    }

    /// <summary>
    /// Gets a user by their email.
    /// </summary>
    /// <param name="email">Email of the user to search.</param>
    /// <returns>The user with the given email.</returns>
    public async Task<User> GetUserByEmailAsync(string email) {
        return await _context.Users.FirstOrDefaultAsync(userQuery => userQuery.Email == email) ?? new User();
    }

    /// <summary>
    /// Saves a user to the database.
    /// </summary>
    /// <param name="user">The user to be added to the database.</param>
    /// <returns>EntityEntry with a User entity saying if the data was entered.</returns>
    public async Task<User> CreateUserAsync(User user) {
        EntityEntry<User> response = await _context.Users.AddAsync(user);

        if (response.State == EntityState.Added) {
            await _context.SaveChangesAsync();

            return response.Entity;
        }

        return new User();
    }
}