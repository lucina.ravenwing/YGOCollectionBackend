﻿using Microsoft.EntityFrameworkCore;
using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Repos;
/// <summary>
/// Database context for YGOCollectionBackend.
/// </summary>
public class YgoDbContext : DbContext {
    
    /// <summary>
    /// Default constructor for YgoDbContext.
    /// </summary>
    public YgoDbContext() {}

    /// <summary>
    /// Dependency injected constructor that calls the base class constructor.
    /// </summary>
    /// <param name="options"></param>
    public YgoDbContext(DbContextOptions<YgoDbContext> options) : base(options) {}

    /// <summary>
    /// Adds repository functionality for the card table in the database and links it to the Card model class.
    /// </summary>
    /// <remarks>See also: <see cref="Card"/></remarks>
    public virtual DbSet<Card> Cards { get; set; } = null!;

    /// <summary>
    /// Adds repository functionality for the card_set table in the database and links it to the CardSet model class.
    /// </summary>
    /// <remarks>See also: <see cref="CardSet"/></remarks>
    public virtual DbSet<CardSet> CardSets { get; set; } = null!;

    /// <summary>
    /// Adds repository functionality for the collection table in the database and links it to the Collection model class.
    /// </summary>
    /// <remarks>See also: <see cref="Collection"/></remarks>
    public virtual DbSet<Collection> Collections { get; set; } = null!;

    /// <summary>
    /// Adds repository functionality for the deck table in the database and links it to the Deck model class.
    /// </summary>
    /// <remarks>See also: <see cref="Deck"/></remarks>
    public virtual DbSet<Deck> Decks { get; set; } = null!;

    /// <summary>
    /// Adds repository functionality for the deck_content table in the database and links it to the DeckContent model class.
    /// </summary>
    /// <remarks>See also <see cref="DeckContent"/></remarks>
    public virtual DbSet<DeckContent> DeckContents { get; set; } = null!;

    /// <summary>
    /// Adds repository functionality for the mdm_meta table in the database and links it to the MdmMetum model class.
    /// </summary>
    /// <remarks>See also: <see cref="MdmMetum"/></remarks>
    public virtual DbSet<MdmMetum> MdmMeta { get; set; } = null!;

    /// <summary>
    /// Adds repository functionality for the users table in the database and links it to the User model class.
    /// </summary>
    /// <remarks>See also: <see cref="User"/></remarks>
    public virtual DbSet<User> Users { get; set; } = null!;

    /// <summary>
    /// Adds repository functionality for the wishlist table in the database and links it to the Wishlist model class.
    /// </summary>
    /// <remarks>See also: <see cref="Wishlist"/></remarks>
    public virtual DbSet<Wishlist> Wishlists { get; set; } = null!;

    /// <summary>
    /// Adds repository functionality for the ygo_pro_deck_meta table in the database and links it to the YgoProDeckMetum model class.
    /// </summary>
    /// <remarks>See also: <see cref="YgoProDeckMetum"/></remarks>
    public virtual DbSet<YgoProDeckMetum> YgoProDeckMeta { get; set; } = null!;

    /// <summary>
    /// Overridden OnModelCreating method to set up all the entities with their given columns.
    /// </summary>
    /// <param name="modelBuilder"></param>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
     {
         modelBuilder.Entity<Card>(entity =>
         {
             entity.HasKey(e => e.Id).HasName("card_pkey");
    
             entity.ToTable("card");
    
             entity.Property(e => e.Id)
                 .ValueGeneratedNever()
                 .HasColumnName("id");
             entity.Property(e => e.Atk).HasColumnName("atk");
             entity.Property(e => e.Attribute).HasColumnName("attribute");
             entity.Property(e => e.CardText).HasColumnName("card_text");
             entity.Property(e => e.CardType).HasColumnName("card_type");
             entity.Property(e => e.Def).HasColumnName("def");
             entity.Property(e => e.GoatLimitations).HasColumnName("goat_limitations");
             entity.Property(e => e.ImageUrls).HasColumnName("image_urls");
             entity.Property(e => e.Level).HasColumnName("level");
             entity.Property(e => e.LinkMarkers).HasColumnName("link_markers");
             entity.Property(e => e.LinkValue).HasColumnName("link_value");
             entity.Property(e => e.MasterDuelRarity)
                 .HasDefaultValueSql("''::text")
                 .HasColumnName("master_duel_rarity");
             entity.Property(e => e.Name).HasColumnName("name");
             entity.Property(e => e.OcgLimitations).HasColumnName("ocg_limitations");
             entity.Property(e => e.Scale).HasColumnName("scale");
             entity.Property(e => e.SortOrder).HasColumnName("sort_order");
             entity.Property(e => e.TcgLimitations).HasColumnName("tcg_limitations");
             entity.Property(e => e.TreatedAs).HasColumnName("treated_as");
             entity.Property(e => e.TypeCategory).HasColumnName("type_category");
         });
    
         modelBuilder.Entity<CardSet>(entity =>
         {
             entity.HasKey(e => new { e.CardSetCode, e.CardSetRarityCode }).HasName("card_set_pkey");
    
             entity.ToTable("card_set");
    
             entity.Property(e => e.CardSetCode).HasColumnName("card_set_code");
             entity.Property(e => e.CardSetRarityCode).HasColumnName("card_set_rarity_code");
             entity.Property(e => e.CardId).HasColumnName("card_id");
             entity.Property(e => e.CardSetPrice).HasColumnName("card_set_price");
             entity.Property(e => e.CardSetRarity).HasColumnName("card_set_rarity");
             entity.Property(e => e.LastUpdated).HasColumnName("last_updated");
             entity.Property(e => e.SetName).HasColumnName("set_name");
    
             entity.HasOne(d => d.Card).WithMany(p => p.CardSets)
                 .HasForeignKey(d => d.CardId)
                 .HasConstraintName("fkfy0ikmefkqnrlg442fe9jyikm");
         });
    
         modelBuilder.Entity<Collection>(entity => {
             entity.HasKey(e => e.Id).HasName("collection_pkey");
                 
             entity.ToTable("collection");
    
             entity.Property(e => e.Id).HasColumnName("id");
             entity.Property(e => e.CardSetCode).HasColumnName("card_set_code");
             entity.Property(e => e.CardSetRarityCode).HasColumnName("card_set_rarity_code");
             entity.Property(e => e.Count).HasColumnName("count");
             entity.Property(e => e.UserId).HasColumnName("user_id");
             entity.Property(e => e.Location).HasColumnName("location");
             entity.Property(e => e.Notes).HasColumnName("notes");

             entity.HasOne(d => d.CardSet).WithMany()
                 .HasForeignKey(d => new { d.CardSetCode, d.CardSetRarityCode })
                 .HasConstraintName("collection_card_set_code_card_set_rarity_code_fkey");
         });
    
         modelBuilder.Entity<Deck>(entity =>
         {
             entity.HasKey(e => e.Id).HasName("deck_pkey");
    
             entity.ToTable("deck");
    
             entity.Property(e => e.Id).HasColumnName("id");
             entity.Property(e => e.DeckName).HasColumnName("deck_name");
             entity.Property(e => e.UserId).HasColumnName("user_id");
    
             entity.HasOne(d => d.User).WithMany(p => p.Decks)
                 .HasForeignKey(d => d.UserId)
                 .HasConstraintName("deck_user_id_fkey");
         });
    
         modelBuilder.Entity<DeckContent>(entity => {
             entity.HasKey(e => new { e.DeckId, e.Position }).HasName("deck_contents_pkey");
                 
             entity.ToTable("deck_contents");
             
             entity.Property(e => e.CardSetCode).HasColumnName("card_set_code");
             entity.Property(e => e.CardSetRarityCode).HasColumnName("card_set_rarity_code");
             entity.Property(e => e.Position).HasColumnName("position");
             entity.Property(e => e.DeckId).HasColumnName("deck_id");
    
             entity.HasOne(d => d.CardSet).WithMany()
                 .HasForeignKey(d => new { d.CardSetCode, d.CardSetRarityCode })
                 .HasConstraintName("deck_contents_card_set_code_card_set_rarity_code_fkey");
         });
    
         modelBuilder.Entity<MdmMetum>(entity =>
         {
             entity.HasKey(e => e.Id).HasName("mdm_meta_pkey");
    
             entity.ToTable("mdm_meta");
    
             entity.Property(e => e.Id).HasColumnName("id");
             entity.Property(e => e.LastUpdated).HasColumnName("last_updated");
         });
    
         modelBuilder.Entity<User>(entity =>
         {
             entity.HasKey(e => e.Id).HasName("users_pkey");
    
             entity.ToTable("users");
    
             entity.Property(e => e.Id).HasColumnName("id");
             entity.Property(e => e.Username).HasColumnName("username");
             entity.Property(e => e.Email).HasColumnName("email");
         });
    
         modelBuilder.Entity<Wishlist>(entity => {
             entity.HasKey(e => e.Id).HasName("wishlist_pkey");
             
             entity.ToTable("wishlist");

             entity.Property(e => e.Id).HasColumnName("id");
             entity.Property(e => e.CardSetCode).HasColumnName("card_set_code");
             entity.Property(e => e.CardSetRarityCode).HasColumnName("card_set_rarity_code");
             entity.Property(e => e.Count).HasColumnName("count");
             entity.Property(e => e.UserId).HasColumnName("user_id");
             entity.Property(e => e.OrderDate).HasColumnName("order_date");
             entity.Property(e => e.Shipped).HasColumnName("shipped");
             entity.Property(e => e.Delivered).HasColumnName("delivered");
             entity.Property(e => e.Notes).HasColumnName("notes");

             entity.HasOne(d => d.CardSet).WithMany()
                 .HasForeignKey(d => new { d.CardSetCode, d.CardSetRarityCode })
                 .HasConstraintName("wishlist_card_set_code_card_set_rarity_code_fkey");
         });
    
         modelBuilder.Entity<YgoProDeckMetum>(entity =>
         {
             entity.HasKey(e => e.Id).HasName("ygo_pro_deck_meta_pkey");
    
             entity.ToTable("ygo_pro_deck_meta");
    
             entity.Property(e => e.Id).HasColumnName("id");
             entity.Property(e => e.AvgTime).HasColumnName("avg_time");
             entity.Property(e => e.Format).HasColumnName("format");
             entity.Property(e => e.TimesRun).HasColumnName("times_run");
         });
         modelBuilder.HasSequence("hibernate_sequence");
     }
}
