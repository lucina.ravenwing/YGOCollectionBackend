using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Responses;

/// <summary>
/// Information about full list of card sets available for the given card.
/// </summary>
public class DeckContentVm : DeckContent {

    /// <summary>
    /// The list of card sets for the given card in the deck.
    /// </summary>
    public List<CardSet> CardSets { get; set; } = new();

    /// <summary>
    /// Constructor that takes in a Deck Content object, sets the current values to the same as the passed in object
    /// then pulls the data about the rest of the card sets for use in the view.
    /// </summary>
    /// <param name="deckContent"></param>
    public DeckContentVm(DeckContent deckContent) {
        DeckId = deckContent.DeckId;
        CardSetCode = deckContent.CardSetCode;
        CardSetRarityCode = deckContent.CardSetRarityCode;
        Position = deckContent.Position;
        CardSet = deckContent.CardSet;
    }

}