using System.Text.Json.Serialization;

namespace YGOCollectionBackend.Data.Responses; 

/// <summary>
/// Response for the log in endpoint.
/// </summary>
public class LogInResponse {

    /// <summary>
    /// Message of the returned response.
    /// </summary>
    [JsonPropertyName("message")]
    public string Message { get; set; } = "";

    /// <summary>
    /// Token of a successful log in.
    /// </summary>
    [JsonPropertyName("token")]
    public string Token { get; set; } = "";

    /// <summary>
    /// RefreshToken of a successful log in.
    /// </summary>
    [JsonPropertyName("refreshToken")]
    public string RefreshToken { get; set; } = "";

    /// <summary>
    /// Expiration date on the server for the access token.
    /// </summary>
    [JsonPropertyName("expiresAt")]
    public string ExpiresAt { get; set; } = "";

}