using System.Text.Json.Serialization;

namespace YGOCollectionBackend.Data.Responses; 

/// <summary>
/// Response from the auth API. Used to deserialize the JSON object.
/// </summary>
public class UserDataResponse {

    /// <summary>
    /// The Username of the found user.
    /// </summary>
    [JsonPropertyName("username")]
    public string Username { get; set; } = "";
    
    /// <summary>
    /// The Email of the found user.
    /// </summary>
    [JsonPropertyName("email")]
    public string Email { get; set; } = "";
    
    /// <summary>
    /// The message returned from the API.
    /// </summary>
    [JsonPropertyName("message")]
    public string Message { get; set; } = "";

}