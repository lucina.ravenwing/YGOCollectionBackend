using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Data.Responses; 

/// <summary>
/// A transfer object to the front end to give both Deck information as well as the Deck Contents.
/// </summary>
public class DeckDataVm {
    /// <summary>
    /// The information about the Deck itself.
    /// </summary>
    public Deck? Deck { get; set; }
    
    /// <summary>
    /// The information about the cards that the deck contains.
    /// </summary>
    public List<DeckContent>? DeckContents { get; set; }

    /// <summary>
    /// All args constructor for the DeckVm class.
    /// </summary>
    public DeckDataVm(Deck deck, List<DeckContent> deckContents) {
        Deck = deck;
        DeckContents = deckContents;
    }
}