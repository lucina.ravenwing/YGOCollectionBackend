﻿namespace YGOCollectionBackend.Data.Models;

/// <summary>
/// Wishlist model class to pull from database table. Wishlist contains cards that the user would like to buy.
/// </summary>
public class Wishlist {
    /// <summary>
    /// The ID of the Wishlist object.
    /// </summary>
    public long Id { get; set; }
    
    /// <summary>
    /// The id of the user that owns the wishlisted item.
    /// </summary>
    public long? UserId { get; set; }

    /// <summary>
    /// The code of the card on the wishlist. Half the FK for CardSet object.
    /// </summary>
    public string? CardSetCode { get; set; }

    /// <summary>
    /// The rarity code of the card on the wishlist. Half the FK for CardSet object.
    /// </summary>
    public string? CardSetRarityCode { get; set; }

    /// <summary>
    /// The count of the wishlisted card wanted.
    /// </summary>
    public short? Count { get; set; }
    
    /// <summary>
    /// The order date of the card on the wishlist.
    /// </summary>
    public DateTime? OrderDate { get; set; }
    
    /// <summary>
    /// Boolean denoting if the card has been shipped.
    /// </summary>
    public bool Shipped { get; set; }
    
    /// <summary>
    /// Boolean denoting if the card has been delivered.
    /// </summary>
    public bool Delivered { get; set; }
    
    /// <summary>
    /// Notes on the wishlisted item. Freeform field.
    /// </summary>
    public string? Notes { get; set; }

    /// <summary>
    /// The CardSet object associated with the FK wishlisted.
    /// </summary>
    /// <remarks>See also: <see cref="CardSet"/></remarks>
    public CardSet? CardSet { get; set; }
}
