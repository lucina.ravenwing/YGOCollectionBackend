﻿namespace YGOCollectionBackend.Data.Models;

/// <summary>
/// Ygo Pro Deck meta data to determine information on how long a certain run takes. Possibly not needed for this app.
/// </summary>
public class YgoProDeckMetum {
    /// <summary>
    /// ID of the table.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Average run time of all the previous times.
    /// </summary>
    public short? AvgTime { get; set; }

    /// <summary>
    /// Total number of times run.
    /// </summary>
    public int? TimesRun { get; set; }

    /// <summary>
    /// An identifier for which action being run to get the proper time.
    /// </summary>
    public string? Format { get; set; }
}
