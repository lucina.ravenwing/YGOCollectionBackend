﻿namespace YGOCollectionBackend.Data.Models;
/// <summary>
/// Card model class to pull information about YGO cards from database.
/// </summary>
public class Card {
    /// <summary>
    /// ID of the table.
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// Name of the card.
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Card Type of the card. Examples: Effect Monster, Spell, Tuner, etc.
    /// </summary>
    public string? CardType { get; set; }

    /// <summary>
    /// Text on the card. Can include effect for effect monsters, spells, and traps and flavor text for vanillas.
    /// </summary>
    public string? CardText { get; set; }

    /// <summary>
    /// Attack of the card.
    /// </summary>
    public int? Atk { get; set; }

    /// <summary>
    /// Defense of the card.
    /// </summary>
    public int? Def { get; set; }

    /// <summary>
    /// Level of the card.
    /// </summary>
    public short? Level { get; set; }

    /// <summary>
    /// Type category of the card. Meaning extra information or type of a monster. "Normal" for a normal trap, Psychic monster.
    /// </summary>
    public string? TypeCategory { get; set; }

    /// <summary>
    /// Attribute of the card.
    /// </summary>
    public string? Attribute { get; set; }

    /// <summary>
    /// Pendulum scale of the card
    /// </summary>
    public short? Scale { get; set; }

    /// <summary>
    /// Link value of the card.
    /// </summary>
    public short? LinkValue { get; set; }

    /// <summary>
    /// Link markers on the card.
    /// </summary>
    public string? LinkMarkers { get; set; }

    /// <summary>
    /// List of Image URLs associated with the card. This could be more than 1 for alt arts.
    /// </summary>
    public string? ImageUrls { get; set; }

    /// <summary>
    /// Information on the car regarding the TCG Forbidden/Limited list.
    /// </summary>
    public string? TcgLimitations { get; set; }

    /// <summary>
    /// Information on the car regarding the OCG Forbidden/Limited list.
    /// </summary>
    public string? OcgLimitations { get; set; }

    /// <summary>
    /// Information on the car regarding the Goat format Forbidden/Limited list.
    /// </summary>
    public string? GoatLimitations { get; set; }

    /// <summary>
    /// The name a card is treated as while in the deck. Necessary for deck building as you can only have 3 cards with the same name in deck.
    /// </summary>
    public string? TreatedAs { get; set; }

    /// <summary>
    /// Order that the cards get sorted based on their CardType.
    /// </summary>
    public short? SortOrder { get; set; }

    /// <summary>
    /// Rarity of the card in the Master Duel video game.
    /// </summary>
    public string? MasterDuelRarity { get; set; }

    /// <summary>
    /// List of sets that the card can be found in.
    /// </summary>
    /// <remarks>See also: <see cref="CardSet"/></remarks>
    public IEnumerable<CardSet> CardSets { get; set; } = new List<CardSet>();

    /// <summary>
    /// Default constructor for Card object. Sets ID to 0 for easier reference.
    /// </summary>
    public Card() {
        Id = 0;
    }
}
