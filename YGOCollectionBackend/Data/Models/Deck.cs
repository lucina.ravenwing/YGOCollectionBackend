﻿using System.Text.Json.Serialization;

namespace YGOCollectionBackend.Data.Models;

/// <summary>
/// Deck model class to pull deck information from database table. Contains the id of the owning User and the name of the Deck.
/// </summary>
public class Deck {
    /// <summary>
    /// The id of the Deck.
    /// </summary>
    public long Id { get; set; }

    /// <summary>   
    /// The ID of the owning user.
    /// </summary>
    public long? UserId { get; set; }

    /// <summary>
    /// The name given to the deck.
    /// </summary>
    public string? DeckName { get; set; }

    /// <summary>
    /// The user object related to the UserId. Does not get passed in return JSON.
    /// </summary>
    /// <remarks>See also: <see cref="User"/></remarks>
    [JsonIgnore]
    public User? User { get; set; }

    /// <summary>
    /// Default constructor for the Deck class. Sets DeckName to blank and UserId to 0 to make comparing easier.
    /// </summary>
    public Deck() {
        DeckName = "";
        UserId = 0;
    }
}
