﻿using System.ComponentModel.DataAnnotations.Schema;

namespace YGOCollectionBackend.Data.Models;

/// <summary>
/// Collection model class to store cards that a user owns.
/// </summary>
public class Collection {
    /// <summary>
    /// The id of the collection object.
    /// </summary>
    public long Id { get; set; }
    
    /// <summary>
    /// The id of the user that owns the card in the collection.
    /// </summary>
    public long? UserId { get; set; }

    /// <summary>
    /// The code of the card in the collection. Half the FK referencing CardSet.
    /// </summary>
    public string? CardSetCode { get; set; }

    /// <summary>
    /// The rarity code of the card in the collection. Half the FK referencing CardSet.
    /// </summary>
    public string? CardSetRarityCode { get; set; }

    /// <summary>
    /// The count of the cards the user owns.
    /// </summary>
    public short? Count { get; set; }
    
    /// <summary>
    /// Where the card is physically.
    /// </summary>
    public string? Location { get; set; }
    
    /// <summary>
    /// Notes about the card. Freeform field.
    /// </summary>
    public string? Notes { get; set; }

    /// <summary>
    /// The actual CardSet object referenced in the collection.
    /// </summary>
    /// <remarks>See also: <see cref="CardSet"/></remarks>
    public CardSet? CardSet { get; set; }
    
    /// <summary>
    /// The actual User object who owns the collection.
    /// </summary>
    /// <remarks>See also: <see cref="User"/></remarks>
    [ForeignKey(nameof(UserId))]
    public User? User { get; set; }
}
