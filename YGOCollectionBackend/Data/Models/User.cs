﻿namespace YGOCollectionBackend.Data.Models;

/// <summary>
/// Data for a User in the application. Most of the user data is handled in the Auth app, but to save some time,
/// a few things are stored here.
/// </summary>
public class User {
    /// <summary>
    /// The user ID.
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// The user's username.
    /// </summary>
    public string? Username { get; set; }
    
    /// <summary>
    /// The user's email.
    /// </summary>
    public string? Email { get; set; }

    /// <summary>
    /// The collection of decks that a user owns. This is pulled from the database and not an actual column.
    /// </summary>
    /// <remarks>See also: <see cref="Deck"/></remarks>
    public IEnumerable<Deck> Decks { get; } = new List<Deck>();

    /// <summary>
    /// Default constructor for User. Sets username to blank and ID to 0 for easier comparisons.
    /// </summary>
    public User() {
        Username = "";
        Id = 0;
    }
}
