﻿using Microsoft.EntityFrameworkCore;

namespace YGOCollectionBackend.Data.Models;

/// <summary>
/// Deck Content model to pull information from database table. Includes cards in a given deck.
/// </summary>
[PrimaryKey(nameof(DeckId), nameof(Position))]
public class DeckContent {
    
    /// <summary>
    /// The ID of the deck that the card pertains to.
    /// </summary>
    public long? DeckId { get; set; }

    /// <summary>
    /// The code for the card set that the card relates to. Half the FK for CardSet.
    /// </summary>
    public string? CardSetCode { get; set; }

    /// <summary>
    /// The rarity code for the card in the deck. Half the FK for CardSet.
    /// </summary>
    public string? CardSetRarityCode { get; set; }

    /// <summary>
    /// Position of the card in the display.
    /// </summary>
    public short? Position { get; set; }

    /// <summary>
    /// The CardSet object that the row relates to.
    /// </summary>
    /// <remarks>See also: <see cref="CardSet"/></remarks>
    public CardSet? CardSet { get; set; }
}
