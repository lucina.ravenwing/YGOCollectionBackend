﻿namespace YGOCollectionBackend.Data.Models;

/// <summary>
/// MDM Meta data model class to pull Master Duel Meta data. Possibly not needed in this app.
/// </summary>
public class MdmMetum {
    /// <summary>
    /// ID of the meta data row.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Time that MDM rarity was last updated.
    /// </summary>
    public DateTime? LastUpdated { get; set; }
}
