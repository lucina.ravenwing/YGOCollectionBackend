﻿using Microsoft.EntityFrameworkCore;

namespace YGOCollectionBackend.Data.Models;

/// <summary>
/// Card set model class to pull from database. Card set refers to the set that a card is a part of. This will include pricing and rarity.
/// </summary>
[PrimaryKey(nameof(CardSetCode), nameof(CardSetRarityCode))]
public class CardSet {
    
    /// <summary>
    /// ID of the card being referred to.
    /// </summary>
    public long CardId { get; set; }

    /// <summary>
    /// The name of the set the card is contained in.
    /// </summary>
    
    public string? SetName { get; set; }

    /// <summary>
    /// The (mostly) unique code for a card in a set. If there are 2 different rarities for the same card in the same set, it will be the same code.
    /// In that case, the CardSetRarityCode is also pulled in.
    /// </summary>
    public string CardSetCode { get; set; }

    /// <summary>
    /// The spelled out rarity of the card in the set. (Rare, Super Rare, Secret Rare, etc).
    /// </summary>
    public string? CardSetRarity { get; set; }

    /// <summary>
    /// The price of the card in the set.
    /// </summary>
    public float? CardSetPrice { get; set; }

    /// <summary>
    /// The date and time that the card set row was last updated.
    /// </summary>
    public DateTime? LastUpdated { get; set; }

    /// <summary>
    /// The code of the rarity for the card in the set (R, C, SR, ScR, etc).
    /// </summary>
    public string CardSetRarityCode { get; set; }

    /// <summary>
    /// The card associated with the card set record.
    /// </summary>
    /// <remarks>See also: <see cref="Card"/></remarks>
    public Card? Card { get; set; }

    /// <summary>
    /// Default constructor for CardSet. Sets CardSetCode and CardSetRarityCode to blank instead of null.
    /// </summary>
    public CardSet() {
        CardSetCode = "";
        CardSetRarityCode = "";
    }
}
