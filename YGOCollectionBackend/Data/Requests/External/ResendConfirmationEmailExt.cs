using System.ComponentModel.DataAnnotations;

namespace YGOCollectionBackend.Data.Requests.External; 

/// <summary>
/// Request to the auth database to resend a confirmation email for a given user.
/// </summary>
public class ResendConfirmationEmailExt {
    
    /// <summary>
    /// The email requesting a new token to be generated for.
    /// </summary>
    [Required]
    public string Email { get; set; } = "";
    
    /// <summary>
    /// The origin of the request. Defaults to the current domain.
    /// </summary>
    public string RequestOrigin { get; set; } = "https://ygo.lucinaravenwing.net";

}