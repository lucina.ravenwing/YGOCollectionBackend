using System.ComponentModel.DataAnnotations;

namespace YGOCollectionBackend.Data.Requests.External; 

/// <summary>
/// Request to get user data from auth server.
/// </summary>
public class GetDataRequestExt {
    /// <summary>
    /// Authentication key for the auth server.
    /// </summary>
    [Required]
    public string Key { get; set; } = "";
    /// <summary>
    /// Username to search for.
    /// </summary>
    public string Username { get; set; } = "";
    /// <summary>
    /// Email to search for.
    /// </summary>
    public string Email { get; set; } = "";
}