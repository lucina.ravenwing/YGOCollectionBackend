namespace YGOCollectionBackend.Data.Requests.External;

/// <summary>Request used to sign up a user.</summary>
public class SignUpRequestExt {
    /// <summary>User's email address.</summary>
    /// <example>test@test.com</example>
    public string EmailAddress { get; set; } = "";

    /// <summary>Username chosen by the user. Must be 6 characters long.</summary>
    /// <example>testUser</example>
    public string Username { get; set; } = "";

    /// <summary>User's password. Must contain 1 uppercase, 1 lowercase, 1 number, and 1 special character.</summary>
    /// <example>Password_1</example>
    public string Password { get; set; } = "";

    /// <summary>Confirmation of user's password. Must match password.</summary>
    /// <example>Password_1</example>
    public string ConfirmPassword { get; set; } = "";

    /// <summary>Origin of the request for redirection after email has been confirmed.</summary>
    /// <example>https://ygo.lucinaravenwing.net</example>
    public string RequestOrigin { get; set; } = "https://ygo.lucinaravenwing.net";
}