using System.ComponentModel.DataAnnotations;

namespace YGOCollectionBackend.Data.Requests.External; 

/// <summary>
/// Request sent Auth Service to give a user a reset password token when they have forgotten their password.
/// </summary>
public class ForgotPasswordRequestExt {
    /// <summary>
    /// The username or email that has forgotten password.
    /// </summary>
    /// <example>lucinaravenwing.net@gmail.com</example>
    [Required]
    public string UsernameEmail { get; set; } = "";
    
    /// <summary>Origin of the request for redirection after email has been confirmed.</summary>
    /// <example>https://ygo.lucinaravenwing.net</example>
    public string Origin { get; set; } = "https://ygo.lucinaravenwing.net";
}