namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>
/// A request from the front end to refresh a user's access token.
/// </summary>
public class TokenRefreshVm {
    /// <summary>
    /// The current access token of the user.
    /// </summary>
    public string Token { get; set; } = "";

    /// <summary>
    /// The refresh token used to generate a new access token.
    /// </summary>
    public string RefreshToken { get; set; } = "";
}