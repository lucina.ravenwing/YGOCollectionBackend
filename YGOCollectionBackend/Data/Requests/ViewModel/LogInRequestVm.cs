using System.ComponentModel.DataAnnotations;

namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>Request used to log a user in.</summary>
public class LogInRequestVm {

    /// <summary>Username or email of an account to log into.</summary>
    /// <example>test@gmail.com</example>
    [Required]
    public string UserNameEmail { get; init; } = "";
    /// <summary>Password of the account to log into.</summary>
    /// <example>Password_1</example>
    [Required] 
    public string Password { get; init; } = "";

}