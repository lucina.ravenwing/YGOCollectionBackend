namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>
/// View model that sends the deck name and an array of information about the cards in the deck.
/// </summary>
public class SaveDeckVm {

    /// <summary>
    /// The name of the deck to be saved.
    /// </summary>
    /// <example>testDeck</example>
    public string DeckName { get; set; } = "";

    /// <summary>
    /// The information about the deck contents themselves.
    /// </summary>
    /// <remarks>
    /// <see cref="SaveDeckContentsVm"/>
    /// </remarks>
    public SaveDeckContentsVm[]? DeckContents { get; set; }

}