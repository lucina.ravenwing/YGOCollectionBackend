using System.ComponentModel.DataAnnotations;

namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>
/// Class used to accept a resend confirmation email request.
/// </summary>
public class ResendConfirmationEmailVm {

    /// <summary>
    /// The email address to send the request to.
    /// </summary>
    /// <example>lucinaravenwing.net@gmail.com</example>
    [Required]
    public string EmailAddress { get; set; } = "";
}