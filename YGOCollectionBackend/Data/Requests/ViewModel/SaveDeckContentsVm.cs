namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>
/// View model that contains all the information about the contents of a deck.
/// </summary>
public class SaveDeckContentsVm {

    /// <summary>
    /// The card set code of the card to be added.
    /// </summary>
    /// <example>BLMR-EN062</example>
    public string CardSetCode { get; set; } = "";

    /// <summary>
    /// The rarity code of the card to be added.
    /// </summary>
    /// <example>UR</example>
    public string CardSetRarityCode { get; set; } = "";

    /// <summary>
    /// The position of the card in the deck for display. Main deck is 1-60, Extra deck is 61-75, and side deck is 76-90.
    /// </summary>
    /// <example>10</example>
    public short Position { get; set; } = 0;
    
    /// <summary>
    /// The sub deck that contains the card. M for main, S for side, and E for extra.
    /// </summary>
    /// <example>M</example>
    public char Subdeck { get; set; }

}