using System.ComponentModel.DataAnnotations;

namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>
/// Request sent to give a user a reset password token when they have forgotten their password.
/// </summary>
public class ForgotPasswordVm {
    /// <summary>
    /// The username or email that has forgotten password.
    /// </summary>
    /// <example>lucinaravenwing.net@gmail.com</example>
    [Required] 
    public string UsernameEmail { get; init; } = "";
}