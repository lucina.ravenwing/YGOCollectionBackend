namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>
/// View Model to delete a deck.
/// </summary>
public class DeleteDeckVm {
    /// <summary>
    /// The ID of the deck to be deleted.
    /// </summary>
    /// <example>4</example>
    public long DeckId { get; set; } = 0;
}