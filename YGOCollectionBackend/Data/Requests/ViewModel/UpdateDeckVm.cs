namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>
/// View model that allows a user to update their deck information.
/// </summary>
public class UpdateDeckVm {

    /// <summary>
    /// The ID of the deck to be updated. Needed since the deck name is not a unique key.
    /// </summary>
    /// <example>4</example>
    public long DeckId { get; set; }

    /// <summary>
    /// The name of the deck to be updated. Could be what is already in the database or a new name.
    /// </summary>
    /// <example>testDeck</example>
    public string DeckName { get; set; } = "";

    /// <summary>
    /// The contents of the deck in array format.
    /// </summary>
    public UpdateDeckContentsVm[]? DeckContents { get; set; }
}