namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>
/// The contents of a deck that is being updated.
/// </summary>
public class UpdateDeckContentsVm {
    
    /// <summary>
    /// The ID of the deck that the contents belong.
    /// </summary>
    /// <example>4</example>
    public long DeckId { get; set; }

    /// <summary>
    /// The code of the card that is being added.
    /// </summary>
    /// <example>BLMR-EN062</example>
    public string CardSetCode { get; set; } = "";

    /// <summary>
    /// The code of the rarity of the card being added to the deck.
    /// </summary>
    /// <example>UR</example>
    public string CardSetRarityCode { get; set; } = "";
    
    /// <summary>
    /// The position of the card in the deck for display. Main deck is 1-60, Extra deck is 61-75, and side deck is 76-90.
    /// </summary>
    /// <example>10</example>
    public short Position { get; set; } = 0;
    
    /// <summary>
    /// The sub deck that contains the card. M for main, S for side, and E for extra.
    /// </summary>
    /// <example>M</example>
    public char Subdeck { get; set; }
}