using System.ComponentModel.DataAnnotations;

namespace YGOCollectionBackend.Data.Requests.ViewModel; 

/// <summary>Request used to sign up a user.</summary>
public class SignUpRequestVm {
    /// <summary>User's email address.</summary>
    /// <example>test@test.com</example>
    [Required]
    public string EmailAddress { get; init; } = "";
    /// <summary>Username chosen by the user. Must be 6 characters long.</summary>
    /// <example>testUser</example>
    [Required]
    [MinLength(6)]
    public string Username { get; init; } = "";
    /// <summary>User's password. Must contain 1 uppercase, 1 lowercase, 1 number, and 1 special character.</summary>
    /// <example>Password_1</example>
    [Required] 
    [MinLength(6)]
    public string Password { get; init; } = "";
    /// <summary>Confirmation of user's password. Must match password.</summary>
    /// <example>Password_1</example>
    [Required] 
    [MinLength(6)]
    public string ConfirmPassword { get; init; } = "";
}