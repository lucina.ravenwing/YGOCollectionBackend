# YuGiOh Collection Frontend

## Repository link
https://gitlab.com/lucina.ravenwing/ygocollection-backend

## URL
https://www.ygo.lucinaravenwing.net

## Description
The backend for a Yu-Gi-Oh collection app. The application allows users to compile a list of cards that they own from the Yu-Gi-Oh trading card game and view the approximate worth of their collection. They are also able to build decks that they would like to put together and pull from their collection to approximate how much the deck will cost to build or complete. If the user is logged in, they can save the collection and decks, but are also able to do both activities as a guest. The data is put together in another of my created APIs. The API pulls from YGOProDeck's card database for the core card information, YugiohPrices for the price information, and MasterDuelMeta for the Master Duel rarity.</br>

## Languages
C#

## Resources Used
ASP.NET Core\
Entity Framework\
PostgreSQL\
CI/CD pipeline\
Docker\
nginx

==================