using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog;
using NLog.Web;
using YGOCollectionBackend.Data.Repos;
using YGOCollectionBackend.Data.Repos.Implementations;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.ExceptionHandling;
using YGOCollectionBackend.HttpWrapper;
using YGOCollectionBackend.Services.Implementations;
using YGOCollectionBackend.Services.Interfaces;

Logger logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("App starting.");

const string allowedOrigins = "AllowLucinaOrigins";

try {
    WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

    // Enable CORS for lucinaravenwing.net sites and localhost for front end testing..
    builder.Services.AddCors(options => {
        options.AddPolicy(name: allowedOrigins,
            policy => {
                policy
                    .WithOrigins("https://www.lucinaravenwing.net",
                        "https://www.auth.lucinaravenwing.net",
                        "https://www.ygo.lucinaravenwing.net",
                        "https://lucinaravenwing.net",
                        "https://auth.lucinaravenwing.net",
                        "https://ygo.lucinaravenwing.net",
                        "http://localhost:3000/",
                        "http://localhost:3000")
                    .AllowAnyHeader()
                    .WithMethods("GET", "POST", "PATCH", "DELETE", "OPTIONS")
                    .AllowCredentials();
            });
    });

    // Add services to the container.
    builder.Services.AddDbContext<YgoDbContext>(
        options => options.UseNpgsql(builder.Configuration.GetConnectionString("YGOCollection"))
    );

    // Add exception handler.
    builder.Services.AddMvc(options => { options.Filters.Add(new ExceptionHandlerFilterAttribute()); });

    // Add HttpClient to services
    builder.Services.AddHttpClient<IHttpClient, HttpClientWrapper>();
    builder.Services.AddScoped<IHttpClient, HttpClientWrapper>();

    // Add user defined repos.
    builder.Services.AddScoped<ICardRepo, CardRepoImpl>();
    builder.Services.AddScoped<ICardSetRepo, CardSetRepoImpl>();
    builder.Services.AddScoped<ICollectionRepo, CollectionRepoImpl>();
    builder.Services.AddScoped<IDeckContentRepo, DeckContentRepoImpl>();
    builder.Services.AddScoped<IDeckRepo, DeckRepoImpl>();
    builder.Services.AddScoped<IUserRepo, UserRepoImpl>();
    builder.Services.AddScoped<IWishlistRepo, WishlistRepoImpl>();

    // Add user defined services.
    builder.Services.AddScoped<IAuthService, AuthServiceImpl>();
    builder.Services.AddScoped<IDeckService, DeckServiceImpl>();
    builder.Services.AddScoped<ICardSetService, CardSetServiceImpl>();
    builder.Services.AddScoped<IDeckContentService, DeckContentServiceImpl>();
    builder.Services.AddScoped<IUserService, UserServiceImpl>();
    builder.Services.AddScoped<ICardService, CardServiceImpl>();

    // Set token validation params.
    TokenValidationParameters tokenValidationParameters = new TokenValidationParameters() {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey =
            new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(builder.Configuration.GetSection("JWT:Secret").Value ?? string.Empty)),
        ValidateIssuer = true,
        ValidIssuer = builder.Configuration.GetSection("Jwt:Issuer").Value,
        ValidateAudience = true,
        ValidAudience = builder.Configuration.GetSection("Jwt:Audience").Value,

        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero
    };

    builder.Services.AddSingleton(tokenValidationParameters);

    // Add Authentication
    builder.Services.AddAuthentication(options => {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(options => {
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = tokenValidationParameters;
            options.Events = new JwtBearerEvents {
                OnMessageReceived = context => {
                    if (context.Request.Cookies.ContainsKey("X-Access-Token")) {
                        context.Token = context.Request.Cookies["X-Access-Token"];
                    }

                    return Task.CompletedTask;
                }
            };
        });


    //NLog: Setup NLog for Dependency Injection
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    // Add controllers
    builder.Services.AddControllers();

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(options => {
        options.SwaggerDoc("v1", new OpenApiInfo {
            Version = "v1",
            Title = "LucinaRavenwing.net YGO Collection API",
            Description =
                "An ASP.NET Core Web API for maintaining your Yu-Gi-Oh collection and calculating approximate worth of the given collection." +
                "<br/>Note: Token auth is used for Swagger as Cookie auth is not available. Cookie auth is used when interacting with a front end." +
                "<br/>To run try it out, log in with a valid user and copy the returned token into the Authorize section."
        });
        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme {
            In = ParameterLocation.Header,
            Description = "Please enter token",
            Name = "Authorization",
            Type = SecuritySchemeType.Http,
            BearerFormat = "JWT",
            Scheme = "bearer"
        });
        options.AddSecurityRequirement(new OpenApiSecurityRequirement {
            {
                new OpenApiSecurityScheme {
                    Reference = new OpenApiReference {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                },
                new string[] { }
            }
        });

        string xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";

        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFileName));
    });

    WebApplication app = builder.Build();

    // Configure the HTTP request pipeline.
    app.UseSwagger();
    app.UseSwaggerUI(swagger => {
        swagger.SwaggerEndpoint("/swagger/v1/swagger.json", "LucinaRavenwing.net YGO Collection API");
        swagger.RoutePrefix = "api/swagger/v1";
    });

    app.UseHttpsRedirection();
    app.UseRouting();

    app.UseCors(allowedOrigins);
    app.UseCookiePolicy(new CookiePolicyOptions {
        Secure = CookieSecurePolicy.Always
    });

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    logger.Warn("Program started.");
    app.Run();
} catch (Exception e) {
    logger.Error(e, "Program stopped due to exception in Program.cs.");
    throw;
} finally {
    LogManager.Shutdown();
}