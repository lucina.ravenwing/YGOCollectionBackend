using System.Diagnostics.CodeAnalysis;
using System.Net.Mime;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NLog;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend.Controllers;

/// <summary>
/// Controller used to call into authentication application to handle all things auth.
/// </summary>
[ApiController]
[Route("api/v1/auth")]
[EnableCors("AllowLucinaOrigins")]
public class AuthController : Controller {
    #region Class members.
    private readonly Logger _logger = LogManager.GetCurrentClassLogger();

    private readonly IAuthService _authService;
    private readonly IConfiguration _configuration;
    #endregion

    #region Ctors
    /// <summary>
    /// Dependency injected constructor for AuthController class.
    /// </summary>
    /// <param name="authService">Service that handles all business logic for the AuthController.</param>
    /// <param name="configuration">Configuration of the application. Used to get values from config files.</param>
    public AuthController(IAuthService authService, IConfiguration configuration) {
        _authService = authService;
        _configuration = configuration;
    }
    #endregion

    #region Public class methods.
    /// <summary>
    /// Controller end point used to register a user. Calls into authentication application at https://auth.lucinaravenwing.net/api/auth/sign-up.
    /// </summary>
    /// <param name="signUpRequest">Request with required information to sign up.</param>
    /// <remarks>
    /// Sample cURL request:
    ///
    /// curl -X POST -H "Content-Type: application/json" -d '{"EmailAddress": "test@test.com", "Username": "testUser", "Password": "Password_1", "ConfirmPassword": "Password_1"}' https://ygo.lucinaravenwing.net/api/v1/auth/sign-up
    /// </remarks>
    /// <returns>A status code with proper HTTP status and an informational message.</returns>
    /// <response code = "200">Indicates the user was successfully signed up.</response>
    /// <response code = "400">Indicates one or more of the inputs led to a failed registration.</response>
    [HttpPost("sign-up")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> SignUp([FromBody] SignUpRequestVm signUpRequest) {
        _logger.Debug("Entering sign up endpoint.");

        Tuple<int, string> response = await _authService.SignUpAsync(signUpRequest);

        // If there is an error, return the error message.
        // TODO: 500 errors?
        if (response.Item1 > 399) {
            _logger.Warn("4XX error: " + response.Item2);

            return BadRequest(response.Item2);
        }

        return Ok(response.Item2);
    }

    /// <summary>
    /// Controller end point used to log a user in. Calls into authentication application at https://auth.lucinaravenwing.net/api/auth/log-in
    /// </summary>
    /// <param name="logInRequestVm">Request with required information to log in.</param>
    /// <remarks>
    /// Sample cURL request:
    ///
    /// curl -X POST -H "Content-Type: application/json" -d '{"UserNameEmail": "testUser", "Password": "Password_1"}' https://ygo.lucinaravenwing.net/api/v1/auth/log-in
    /// </remarks>
    /// <returns>A status code with proper HTTP status and an informational message.</returns>
    /// <response code = "200">Indicates the user was successfully logged in.</response>
    /// <response code = "400">Indicates one or more of the inputs led to a failed log in attempt.</response>
    [HttpPost("log-in")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> LogIn([FromBody] LogInRequestVm logInRequestVm) {
        _logger.Debug("Entering log in endpoint.");

        Tuple<int, LogInResponse> response = await _authService.LogInAsync(logInRequestVm);

        // If there is an error, return the proper response.
        // TODO: 500 error?
        if (response.Item1 >= 400) {
            if (response.Item1 == 401) {
                return Unauthorized(response.Item2);
            }

            return BadRequest(response.Item2);
        }

        // Add access and refresh token to the response on a successful log in. HTTP only and secure so the browser and
        // JS can't access it.
        Response.Cookies.Append("X-Access-Token", response.Item2.Token, new CookieOptions {
            HttpOnly = true,
            SameSite = SameSiteMode.None,
            Secure = true,
            Expires = new DateTimeOffset(DateTime.Now.Add(new TimeSpan(0, 0, 20, 0)))
        });
        Response.Cookies.Append("X-Refresh-Token", response.Item2.RefreshToken, new CookieOptions {
            HttpOnly = true,
            SameSite = SameSiteMode.None,
            Secure = true,
            Expires = new DateTimeOffset(DateTime.Now.Add(new TimeSpan(180, 0, 0, 0)))
        });
        
        return Ok(response.Item2);
    }

    /// <summary>
    /// Controller end point used to log a user out. Calls into authentication application at https://auth.lucinaravenwing.net/api/auth/log-out
    /// </summary>
    /// <remarks>
    ///     cURL request not applicable as cookie is required.
    /// </remarks>
    /// <returns>A status code with an HTTP status and message with error or success information.</returns>
    /// <response code = "200">Indicates the user was successfully logged out.</response>
    /// <response code = "400">Indicates the logOutRequest is incorrect.</response>
    [HttpPost("log-out")]
    [Produces(MediaTypeNames.Application.Json)]
    // Suppress typos in the cURL example.
    [SuppressMessage("ReSharper", "CommentTypo")]
    public async Task<IActionResult> LogOut() {
        _logger.Debug("Logging user out.");

        string? refreshToken = Request.Cookies["X-Refresh-Token"];
        
        // Delete the cookies regardless of what happens. This will force a pseudo log out on the client side.
        Response.Cookies.Delete("X-Access-Token");
        Response.Cookies.Delete("X-Refresh-Token");

        // If there's no refersh token present in the cookies, return a 500 error. The user has no direct interaction,
        // so this isn't a 4XX error.
        if (refreshToken == null) {
            return StatusCode(StatusCodes.Status500InternalServerError, "Error: Token not present in cookies.");
        }
        
        Tuple<int, string> response = await _authService.LogOutAsync(refreshToken);
        
        return StatusCode(response.Item1, response.Item2);
    }

    /// <summary>
    /// Endpoint used to resend the confirmation email to a user.
    /// </summary>
    /// <remarks>
    /// Sample cURL request:
    /// 
    ///     curl -X POST -H "Content-Type: application/json" -d '{"EmailAddress": "lucinaravenwing.net@gmail.com"}' https://ygo.lucinaravenwing.net/api/v1/auth/resend-confirmation-email
    /// </remarks>
    /// <param name="resendConfirmationEmailVm">The email of the user requesting a new confirmation email.</param>
    /// <returns>A response to the front end saying whether the email sent successfully or not.</returns>
    [HttpPost("resend-confirmation-email")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> ResendConfirmationEmail(
        [FromBody] ResendConfirmationEmailVm resendConfirmationEmailVm) {
        if (resendConfirmationEmailVm.EmailAddress == "") {
            return BadRequest("Error: Email required.");
        }

        Tuple<int, string> response =
            await _authService.ResendConfirmationEmailAsync(resendConfirmationEmailVm.EmailAddress);

        // Set up return messages based on return code. 
        switch (response.Item1) {
            case >= 400 and < 500:
                return BadRequest(response.Item2);
            case >= 500:
                return StatusCode(StatusCodes.Status500InternalServerError, response.Item2);
            default:
                return Ok(response.Item2);
        }
    }

    /// <summary>
    /// Endpoint used to allow a user that has forgotten their password to get an email to reset it. 
    /// </summary>
    /// <remarks>
    /// Sample cURL request:
    ///
    ///     curl -X POST -H "Content-Type: application/json'" -d '{"UsernameEmail": "lucinaravenwing.net@gmail.com"}' https://ygo.lucinaravenwing.net/api/v1/auth/forgot-password
    /// </remarks>
    /// <param name="forgotPasswordVm">The view model containing information passed in from the user.</param>
    /// <returns>A response denoting if the email was successfully sent or not.</returns>
    [HttpPost("forgot-password")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordVm forgotPasswordVm) {
        if (forgotPasswordVm.UsernameEmail == "") {
            return BadRequest("Please enter a username or email.");
        }

        Tuple<int, string> response = await _authService.ForgotPasswordAsync(forgotPasswordVm);
        
        switch (response.Item1) {
            case >= 400 and < 500:
                return BadRequest(response.Item2);
            case >= 500:
                return StatusCode(StatusCodes.Status500InternalServerError, response.Item2);
            default:
                return Ok(response.Item2);
        }
    }

    /// <summary>
    /// Endpoint used to reset a user's password on the auth server.
    /// </summary>
    /// <remarks>
    /// Sample cURL request:
    ///
    ///     curl -X POST -H "Content-Type: application/json'" -d '{"Token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg", "NewPassword": "FakePassword!", "Email": "lucinaravenwing.net@gmail.com", "Origin": "https://ygo.lucinaravenwing.net" }' https://ygo.lucinaravenwing.net/api/v1/auth/reset-password
    /// </remarks>
    /// <param name="resetPasswordVm"></param>
    /// <returns>A response denoting if the email was successfully sent or not.</returns>
    [HttpPost("reset-password")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordVm resetPasswordVm) {
        Tuple<int, string> result = await _authService.ResetPasswordAsync(resetPasswordVm);

        switch (result.Item1) {
            case >= 400 and < 500:
                return BadRequest(result.Item2);
            case >= 500:
                return StatusCode(StatusCodes.Status500InternalServerError);
            default:
                return Ok(result.Item2);
        }
    }

    /// <summary>
    /// Endpoint to refresh a user's access token.
    /// </summary>
    /// <remarks>
    /// cURL request not applicable as cookie is required.
    /// </remarks>
    /// <returns>A response denoting if the email was successfully sent or not.</returns>
    [HttpPost("token-refresh")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> TokenRefresh() {
        string? refreshCookie = Request.Cookies["X-Refresh-Token"];
        string? accessToken = Request.Cookies["X-Access-Token"];

        TokenRefreshVm tokenRefreshVm = new();

        if (refreshCookie != null) {
            // if the access token is null, grab from the config. This is required because the auth service uses the 
            // access token and it cannot be null.
            accessToken ??= _configuration.GetSection("Misc:dummy-access-token").Value;

            // If the config doesn't work for any reason, return 500 error.
            if (accessToken == null) {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error: Cookie does not contain required information.");
            }
            
            tokenRefreshVm.RefreshToken = refreshCookie;
            tokenRefreshVm.Token = accessToken;
        } else {
            return StatusCode(StatusCodes.Status500InternalServerError, "Error: Cookie does not contain required information.");
        }
        
        Tuple<int, LogInResponse> response = await _authService.TokenRefreshAsync(tokenRefreshVm);

        switch (response.Item1) {
            case >= 400 and < 500:
                return BadRequest(response.Item2);
            case >= 500:
                return StatusCode(StatusCodes.Status500InternalServerError);
        }

        // Add the new tokens to the response cookie.
        Response.Cookies.Append("X-Access-Token", response.Item2.Token, new CookieOptions {
            HttpOnly = true,
            SameSite = SameSiteMode.None,
            Secure = true,
            Expires = new DateTimeOffset(DateTime.Now.Add(new TimeSpan(0, 0, 20, 0)))
        });
        Response.Cookies.Append("X-Refresh-Token", response.Item2.RefreshToken, new CookieOptions {
            HttpOnly = true,
            SameSite = SameSiteMode.None,
            Secure = true,
            Expires = new DateTimeOffset(DateTime.Now.Add(new TimeSpan(180, 0, 0, 0)))
        });

        return Ok(response.Item2);
    }
    #endregion
}