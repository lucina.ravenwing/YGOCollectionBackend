using System.Net.Mime;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NLog;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend.Controllers; 

/// <summary>
/// Controller for deck functionality in the application. Allows the user to build, save, retrieve, etc. a collection of
/// cards.
/// </summary>
///
/// <remarks>See also: <see cref="Data.Models.Deck"/></remarks>
/// <remarks>See also: <see cref="Data.Models.DeckContent"/></remarks>
[ApiController]
[Route("/api/v1/deck")]
[EnableCors("AllowLucinaOrigins")]
public class DeckController : Controller {
    #region Class members.
    private readonly Logger _logger = LogManager.GetCurrentClassLogger();

    private readonly IDeckService _deckService;
    #endregion

    #region Ctors
    /// <summary>
    /// Dependency injected constructor for the DeckController class.
    /// </summary>
    /// <param name="deckService">Service that handles business logic for deck related activities.</param>
    public DeckController(IDeckService deckService) {
        _deckService = deckService;
    }
    #endregion

    #region Public class methods.
    /// <summary>
    /// Endpoint that retrieves a user's decks from the database.
    /// </summary>
    /// <returns>A list of the decks from the user has saved.</returns>
    /// <remarks>
    /// Sample Curl Request:
    /// 
    ///     curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" https://ygo.lucinaravenwing.net/api/deck/get-decks
    /// </remarks>
    ///
    /// <response code="200">Valid request and response. Returns json of decks.</response>
    /// <response code="401">Response indicating a user does not have correct claims to access the data or may not be logged in.</response>
    /// <response code="403">Response indicating the user cannot access this endpoint.</response>
    [HttpGet("get-decks")]
    [Authorize]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> GetDecks() {
        // Get the claims from the context to use for validating the correct user data is being pulled.
        ClaimsIdentity? identity = HttpContext.User.Identity as ClaimsIdentity;

        if (identity?.FindFirst(ClaimTypes.Name)?.Value != null) {
            return Ok(await _deckService.GetDecksAsync(identity));
        }

        _logger.Error("User authorized, but not able to get claims information.");
        return Unauthorized("Error: Not able to get claims information.");
    }

    /// <summary>
    /// Endpoint that saves a deck to the database.
    /// </summary>
    /// <returns>A status and message regarding if the deck was properly saved.</returns>
    /// <remarks>
    /// 
    /// Sample Curl Request:
    ///
    /// curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" -d '{"DeckName": "testDeck",  "DeckContents":  [{ "CardSetCode": "BLMR-EN062", "CardSetRarityCode": "UR", "Position": 3 },  { "CardSetCode": "GFP2-EN113", "CardSetRarityCode": "UR", "Position": 1  } ]}' https://ygo.lucinaravenwing.net/api/deck/save-deck
    /// </remarks>
    /// <response code="200">Response indicating the deck was successfully saved.</response>
    /// <response code="400">Response indicating the data supplied was not in the correct format.</response>
    /// <response code="401">Response indicating the user does not have the right access or may not be logged in.</response>
    /// <response code="403">Response indicating the user cannot access this endpoint.</response>
    [HttpPost("save-deck")]
    [Authorize]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> SaveDeck([FromBody] SaveDeckVm saveDeckVm) {
        // Validate the number of cards in the deck to ensure saving is correct.
        if (saveDeckVm.DeckContents == null || saveDeckVm.DeckContents.Length > 90) {
            return BadRequest("Error: " + (saveDeckVm.DeckContents == null ? "Deck contents are null." : "Deck contents contain too many cards for a legal list."));
        }
        
        // Pull the claims of the user based off the context so the owner is correctly set in the database.
        ClaimsIdentity? identity = HttpContext.User.Identity as ClaimsIdentity;

        if (identity?.FindFirst(ClaimTypes.Name)?.Value == null) {
            return Unauthorized("Error: User does not have any claims.");
        }

        Tuple<int, string> response = await _deckService.SaveDeckAsync(saveDeckVm, identity);

        return StatusCode(response.Item1, response.Item2);

    }

    /// <summary>
    /// Endpoint that updates a deck in the database.
    /// </summary>
    /// <returns>A status and message regarding if the deck was properly updated.</returns>
    /// <remarks>
    /// 
    /// Sample Curl Request:
    ///
    /// curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" -d '{"DeckId": 4, "DeckName": "testDeck",  "DeckContents":  [{ "CardSetCode": "BLMR-EN062", "CardSetRarityCode": "UR", "Position": 3 },  { "CardSetCode": "GFP2-EN113", "CardSetRarityCode": "UR", "Position": 1  } ]}' https://ygo.lucinaravenwing.net/api/deck/update-deck
    /// </remarks>
    /// <response code="200">Response indicating the deck was successfully updated.</response>
    /// <response code="400">Response indicating the data supplied was not in the correct format or the deck doesn't exist.</response>
    /// <response code="401">Response indicating the user does not have the right access, tried to update a deck that does not belong to them, or may not be logged in.</response>
    /// <response code="403">Response indicating the user cannot access this endpoint.</response>
    [HttpPatch("update-deck")]
    [Authorize]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> UpdateDeck([FromBody] UpdateDeckVm updateDeckVm) {
        // Check the size of the deck to ensure update is done correctly.
        if (updateDeckVm.DeckContents == null || updateDeckVm.DeckContents.Length > 90) {
            return BadRequest("Error: " + (updateDeckVm.DeckContents == null ? "Deck contents are null." : "Deck contents contain too many cards for a legal list."));
        }

        // Get the user claims to ensure the database is updated correctly and the requesting user owns the deck.
        ClaimsIdentity? identity = HttpContext.User.Identity as ClaimsIdentity;

        if (identity?.FindFirst(ClaimTypes.Name)?.Value != null) {
            Tuple<int, string> response = await _deckService.UpdateDeckAsync(updateDeckVm, identity);

            return StatusCode(response.Item1, response.Item2);
        }

        return Unauthorized("Error: User does not have any claims.");
    }

    /// <summary>
    /// Deletes a user's deck by the given deck ID.
    /// </summary>
    /// <param name="deckVm">The ID of the deck to be deleted.</param>
    /// <returns>A response with the success or failure of the request.</returns>
    /// <remarks>
    /// 
    /// Sample Curl Request:
    /// 
    /// curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" -d '{"DeckId": 4}' https://ygo.lucinaravenwing.net/api/deck/delete-deck
    /// </remarks>
    /// <response code="200">Response indicating the deck was successfully deleted.</response>
    /// <response code="400">Response indicating the data supplied was not in the correct format or the deck doesn't exist.</response>
    /// <response code="401">Response indicating the user does not have the right access, tried to update a deck that does not belong to them, or may not be logged in.</response>
    /// <response code="403">Response indicating the user cannot access this endpoint.</response>
    [HttpDelete("delete-deck")]
    [Authorize]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> DeleteDeck([FromBody] DeleteDeckVm deckVm) {
        // Get the claims to ensure the user owns the deck that is attempting to be deleted.
        ClaimsIdentity? identity = HttpContext.User.Identity as ClaimsIdentity;

        if (identity?.FindFirst(ClaimTypes.Name)?.Value == null) {
            return Unauthorized("Error: User does not have any claims.");
        }

        Tuple<int, string> response = await _deckService.DeleteDeckAsync(deckVm.DeckId, identity);

        return StatusCode(response.Item1, response.Item2);
    }
    #endregion
}