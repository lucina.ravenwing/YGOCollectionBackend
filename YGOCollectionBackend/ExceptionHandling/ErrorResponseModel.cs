namespace YGOCollectionBackend.ExceptionHandling; 

/// <summary>
/// Generic response with a message for returning errors.
/// </summary>
public class ErrorResponseModel {
    /// <summary>
    /// Message returned to the user.
    /// </summary>
    public string Message { get; init; } = "";
}