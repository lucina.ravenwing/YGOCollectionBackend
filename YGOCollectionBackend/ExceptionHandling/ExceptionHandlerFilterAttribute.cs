using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NLog;
using NLog.Web;

namespace YGOCollectionBackend.ExceptionHandling;

/// <summary>
/// Exception handler for 500 server errors.
/// </summary>
public class ExceptionHandlerFilterAttribute : ExceptionFilterAttribute {
    private readonly Logger _logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
    
    /// <summary>
    /// Catch the exception and handle it.
    /// </summary>
    /// <param name="context">Context of the thrown exception.</param>
    public override void OnException(ExceptionContext context) {
        ErrorResponseModel error = new() {
            Message = "An error has occurred."
        };

        Exception exception = context.Exception;
        context.HttpContext.Response.StatusCode = 500;
        
        _logger.Error($"There was an error. Type: {exception.GetType()}. Message {exception.Message}" );

        if (exception is ApplicationException) {
            _logger.Error("Error occured: " + error.Message);
            
            context.HttpContext.Response.StatusCode = 400;
        }

        context.Result = new ObjectResult(error);
    }
}