using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend.Services.Implementations;

/// <summary>
/// Implementation of UserService. Contains business logic for dealing with user data.
/// </summary>
public class UserServiceImpl : IUserService {

    #region Class members
    private readonly IUserRepo _userRepo;
    #endregion

    #region Ctors
    /// <summary>
    /// Dependency injected constructor for the UserServiceImpl class.
    /// </summary>
    /// <param name="userRepo">The user repository for connecting to the database.</param>
    public UserServiceImpl(IUserRepo userRepo) {
        _userRepo = userRepo;
    }
    #endregion

    #region Public class methods
    /// <summary>
    /// Gets the user of the application that has the given username from the repository.
    /// </summary>
    /// <param name="username">The username to search.</param>
    /// <returns>The user with the given username or a blank user if one is not found.</returns>
    public async Task<User> GetUserByUsernameAsync(string username) {
        return await _userRepo.GetUserByUsernameAsync(username);
    }

    /// <summary>
    /// Gets the user of the application that has the given email from the repository.
    /// </summary>
    /// <param name="email">The email to search.</param>
    /// <returns>The user with the given email or a blank user if one is not found.</returns>
    public async Task<User> GetUserByEmailAsync(string email) {
        return await _userRepo.GetUserByEmailAsync(email);
    }

    /// <summary>
    /// Sends information to the User repository to store a new User in the database.
    /// </summary>
    /// <param name="newUser">A User object that will be stored into the database.</param>
    /// <returns>The user that was created in the database or a blank User if it failed to create.</returns>
    public async Task<User> CreateUserAsync(User newUser) {
        return await _userRepo.CreateUserAsync(newUser);
    }
    #endregion
    
}