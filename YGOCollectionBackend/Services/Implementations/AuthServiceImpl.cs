using System.Text;
using System.Text.Json;
using NLog;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Requests.External;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;
using YGOCollectionBackend.HttpWrapper;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend.Services.Implementations;

/// <summary>
/// Implementation of AuthService. Contains the business logic for authentication in the app.
/// </summary>
public class AuthServiceImpl : IAuthService {
    #region Class members
    private readonly Logger _logger = LogManager.GetCurrentClassLogger();

    private readonly IHttpClient _client;
    private readonly IUserService _userService;

    private readonly string? _getDataKey;
    #endregion

    #region Ctors
    /// <summary>
    /// Dependency injected constructor for AuthServiceImplClass.
    /// </summary>
    /// <param name="client">An HttpClientWrapper used to make calls to API.</param>
    /// <param name="userService">A service to handle all calls into the user repository.</param>
    /// <param name="configuration">Configuration for the application. Used to pull data from appsettings.</param>
    public AuthServiceImpl(IHttpClient client, IUserService userService, IConfiguration configuration) {
        _client = client;
        _userService = userService;

        _getDataKey = configuration.GetSection("Misc:get-user-data-key").Value;

        _getDataKey ??= "";
    }
    #endregion

    #region Public class methods
    /// <summary>
    /// Implementation of the SignUpAsync function. Calls into the authentication application to create a new user.
    /// </summary>
    /// <param name="signUpRequest">Request with required information to create a new user.</param>
    /// <returns>Details about the status of the user creation.</returns>
    public async Task<Tuple<int, string>> SignUpAsync(SignUpRequestVm signUpRequest) {
        _logger.Debug("Entered sign up service method.");

        const string url = "https://auth.lucinaravenwing.net/api/auth/sign-up";

        StringContent body = new(JsonSerializer.Serialize(new SignUpRequestExt {
            Username = signUpRequest.Username,
            EmailAddress = signUpRequest.EmailAddress,
            Password = signUpRequest.Password,
            ConfirmPassword = signUpRequest.ConfirmPassword,
            RequestOrigin = "https://ygo.lucinaravenwing.net"
        }), Encoding.UTF8, "application/json");

        HttpResponseMessage response = await _client.PostAsync(url, body);

        _logger.Debug("Client called");

        // Get the response from the post call and clean up the starting and ending quotation marks.
        string responseString = await response.Content.ReadAsStringAsync();
        
        responseString = responseString[0] == '\"'
            ? responseString.Substring(1, responseString.Length - 1)
            : responseString;
        responseString = responseString[^1] == '\"'
            ? responseString.Substring(0, responseString.Length - 1)
            : responseString;

        if ((int)response.StatusCode < 400) {
            bool userCreated = await CreateUser(signUpRequest);

            if (userCreated) {
                _logger.Info("User created.");
            }
        }

        // Return a Tuple to contain both the status code to return to the user and the message.
        return new Tuple<int, string>((int)response.StatusCode, responseString);
    }

    private async Task<bool> CreateUser(SignUpRequestVm signUpRequestVm) {
        User response = await _userService.CreateUserAsync(new User {
            Username = signUpRequestVm.Username,
            Email = signUpRequestVm.EmailAddress
        });

        if (response.Username == "") {
            _logger.Error("User with username " + signUpRequestVm.Username + " could not be created.");
        }

        // Returns whether the user was successfully created or not.
        return response.Username != "";
    }

    /// <summary>
    /// Definition of the LogInAsync function.
    /// </summary>
    /// <param name="logInRequestVm">Request with information needed to a log a user in.</param>
    /// <returns>Details about the status of the login attempt.</returns>
    public async Task<Tuple<int, LogInResponse>> LogInAsync(LogInRequestVm logInRequestVm) {
        _logger.Debug("Entered log in service method.");

        const string url = "https://auth.lucinaravenwing.net/api/auth/log-in";

        StringContent body = new(JsonSerializer.Serialize(logInRequestVm), Encoding.UTF8, "application/json");

        HttpResponseMessage response = await _client.PostAsync(url, body);

        _logger.Debug("Client called.");

        // Get the response and clean up the quotation marks.
        string responseString = await response.Content.ReadAsStringAsync();
        responseString = responseString[0] == '\"'
            ? responseString.Substring(1, responseString.Length - 1)
            : responseString;
        responseString = responseString[^1] == '\"'
            ? responseString.Substring(0, responseString.Length - 1)
            : responseString;

        LogInResponse logInResponse = new() {
            Message = responseString
        };

        if ((int)response.StatusCode < 400) {
            User user;
            string email = "";
            string username = "";

            // Set either the username or email field depending on if the string contains an @ symbol.
            if (!logInRequestVm.UserNameEmail.Contains('@')) {
                user = await _userService.GetUserByUsernameAsync(logInRequestVm.UserNameEmail);
                username = logInRequestVm.UserNameEmail;
            }
            else {
                user = await _userService.GetUserByEmailAsync(logInRequestVm.UserNameEmail);
                email = logInRequestVm.UserNameEmail;
            }
            
            
            if (user.Username != "") {
                _logger.Info("User exists.");
            }

            // If the user doesn't exist in the current system, checks if it exists in the auth server first
            // before saying that the user doesn't exist at all.
            if (user.Id == 0 || user.Username == "" || user.Email == "") {
                _logger.Info("User data not present in current systme. Polling auth server.");

                const string getDataUrl = "https://auth.lucinaravenwing.net/api/user/get-user-data";

                GetDataRequestExt getDataRequest = new() {
                    Key = _getDataKey ?? string.Empty,
                    Email = email,
                    Username = username
                };

                StringContent getDataBody = new(JsonSerializer.Serialize(getDataRequest), Encoding.UTF8,
                    "application/json");
                HttpResponseMessage getDataResponse = await _client.PostAsync(getDataUrl, getDataBody);

                UserDataResponse? userData =
                    JsonSerializer.Deserialize<UserDataResponse>(await getDataResponse.Content.ReadAsStringAsync());

                // If the request was successful and all the user data is not null, create the user in the current system.
                if (getDataResponse.StatusCode.ToString() == "OK" && userData != null &&
                    !string.IsNullOrEmpty(userData.Email)
                    && !string.IsNullOrEmpty(userData.Username)) {

                    user = new User {
                        Email = userData.Email,
                        Username = userData.Username
                    };

                    User returnedUser = await _userService.CreateUserAsync(user);

                    if (returnedUser.Username != user.Username && returnedUser.Email != user.Email) {
                        _logger.Error("Error when saving user to ygo database.");
                    }
                    else {
                        _logger.Info("User properly saved.");
                    }
                }
                else {
                    _logger.Error("Error when saving user to ygo database.");
                }
            }

            logInResponse =
                JsonSerializer.Deserialize<LogInResponse>(responseString) ?? new LogInResponse();
        }

        // Return Tuple of the status code and the response object so the controller has all the information.
        return new Tuple<int, LogInResponse>((int)response.StatusCode, logInResponse);
    }

    /// <summary>
    /// Implementation of the LogOutAsync method. Requests the user be logged out on the auth server.
    /// </summary>
    /// <param name="refreshToken">Request with the user's refresh token to be logged out.</param>
    /// <returns>Tuple with information about the success or failure of the request.</returns>
    public async Task<Tuple<int, string>> LogOutAsync(string refreshToken) {
        const string requestUrl = "https://auth.lucinaravenwing.net/api/auth/log-out";

        LogOutRequestVm logOutRequestVm = new () {
            RefreshToken = refreshToken
        };

        StringContent bodyData = new(JsonSerializer.Serialize(logOutRequestVm), Encoding.UTF8, "application/json");
        HttpResponseMessage response = await _client.PostAsync(requestUrl, bodyData);
        string responseMessage = await response.Content.ReadAsStringAsync();

        // Return a tuple of the status and the message so the controller has all the information.
        return new Tuple<int, string>((int)response.StatusCode, responseMessage);
    }

    /// <summary>
    /// Implementation of the ResendConfirmationEmailAsync method. Sends a new confirmation email to requested email address.
    /// </summary>
    /// <param name="email">Email address to resend email to.</param>
    /// <returns>Tuple with information about the success or failure of the request.</returns>
    public async Task<Tuple<int, string>> ResendConfirmationEmailAsync(string email) {
        const string requestUrl = "https://auth.lucinaravenwing.net/api/user/resend-confirm-email";

        ResendConfirmationEmailExt resendConfirmationEmailRequest = new() {
            Email = email,
            RequestOrigin = "https://ygo.lucinaravenwing.net"
        };

        StringContent getDataBody = new(JsonSerializer.Serialize(resendConfirmationEmailRequest), Encoding.UTF8,
            "application/json");
        HttpResponseMessage response = await _client.PostAsync(requestUrl, getDataBody);
        string responseMessage = await response.Content.ReadAsStringAsync();

        // Return the status code and message in a tuple so the controller doesn't need to modify the data.
        return new Tuple<int, string>((int)response.StatusCode, responseMessage);
    }

    /// <summary>
    /// Service to send a request to the auth service to send a password reset email to the specified username/email.
    /// </summary>
    /// <param name="forgotPasswordVm"></param>
    /// <returns>Tuple containing the status and the message.</returns>
    public async Task<Tuple<int, string>> ForgotPasswordAsync(ForgotPasswordVm forgotPasswordVm) {
        const string requestUrl = "https://auth.lucinaravenwing.net/api/auth/forgot-password";

        ForgotPasswordRequestExt forgotPasswordRequest = new() {
            UsernameEmail = forgotPasswordVm.UsernameEmail,
            Origin = "https://ygo.lucinaravenwing.net"
        };

        StringContent getDataBody = new(JsonSerializer.Serialize(forgotPasswordRequest), Encoding.UTF8,
            "application/json");

        HttpResponseMessage response = await _client.PostAsync(requestUrl, getDataBody);
        string responseMessage = await response.Content.ReadAsStringAsync();

        // return a tuple of the response code and the message so the controller doesn't need to modify data.
        return new Tuple<int, string>((int)response.StatusCode, responseMessage);
    }

    /// <summary>
    /// Resets a users password on the auth server.
    /// </summary>
    /// <param name="resetPasswordVm">The request view model that contains the token, the user's email, the new password,
    /// and the origin on the request.</param>
    /// <returns>A tuple of the response code and message to be returned to the front end.</returns>
    public async Task<Tuple<int, string>> ResetPasswordAsync(ResetPasswordVm resetPasswordVm) {
        const string requestUrl = "https://auth.lucinaravenwing.net/api/user/reset-password";

        StringContent body = new(JsonSerializer.Serialize(resetPasswordVm), Encoding.UTF8,
            "application/json");

        HttpResponseMessage response = await _client.PostAsync(requestUrl, body);
        string responseMessage = await response.Content.ReadAsStringAsync();
        
        return new Tuple<int, string>((int)response.StatusCode, responseMessage);
    }

    /// <summary>
    /// Refreshes a user's access token through the auth server.
    /// </summary>
    /// <param name="tokenRefreshVm">The refresh request containing the current token and the refresh token.</param>
    /// <returns>A tuple of the response code and message to be returned to the front end.</returns>
    public async Task<Tuple<int, LogInResponse>> TokenRefreshAsync(TokenRefreshVm tokenRefreshVm) {
        const string requestUrl = "https://auth.lucinaravenwing.net/api/auth/refresh-token";

        StringContent body = new(JsonSerializer.Serialize(tokenRefreshVm), Encoding.UTF8,
            "application/json");

        HttpResponseMessage response = await _client.PostAsync(requestUrl, body);
        string responseMessage = await response.Content.ReadAsStringAsync();
        
        LogInResponse logInResponse =
            JsonSerializer.Deserialize<LogInResponse>(responseMessage) ?? new LogInResponse();
        
        return new Tuple<int, LogInResponse>((int)response.StatusCode, logInResponse);
    }
    #endregion
}