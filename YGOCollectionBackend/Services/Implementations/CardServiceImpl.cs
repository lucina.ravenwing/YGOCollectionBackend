using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend.Services.Implementations;

/// <summary>
/// Implementation of CardService. Contains business logic for retrieving and storing card data.
/// </summary>
///
/// <remarks>See also: <see cref="Data.Models.Card"/></remarks>
public class CardServiceImpl : ICardService {

    #region Class members
    private readonly ICardRepo _cardRepo;
    #endregion

    #region Ctors
    /// <summary>
    /// Dependency injected constructor for the CardServiceImpl class.
    /// </summary>
    /// <param name="cardRepo">The card repository for connecting to the database.</param>
    public CardServiceImpl(ICardRepo cardRepo) {
        _cardRepo = cardRepo;
    }
    #endregion

    #region Public class methods
    /// <summary>
    /// Get the card with the given ID from the repository.
    /// </summary>
    /// <param name="cardId">The ID of the card to search.</param>
    /// <returns>The card with the given ID or a blank card if one is not found.</returns>
    public async Task<Card> GetCardByIdAsync(long cardId) {
        return await _cardRepo.GetCardByIdAsync(cardId);
    }
    #endregion
    
}