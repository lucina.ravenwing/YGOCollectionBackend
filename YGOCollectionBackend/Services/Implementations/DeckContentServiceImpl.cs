using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend.Services.Implementations;

/// <summary>
/// Implementation of DeckContentService. Contains business logic for retrieving and storing deck content data.
/// </summary>
public class DeckContentServiceImpl : IDeckContentService {

    #region Class members
    private readonly IDeckContentRepo _deckContentRepo;
    #endregion

    #region Ctors
    /// <summary>
    /// Dependency injected constructor for the DeckContentServiceImpl class.
    /// </summary>
    /// <param name="deckContentRepo">The deck content repository for connecting to the database.</param>
    public DeckContentServiceImpl(IDeckContentRepo deckContentRepo) {
        _deckContentRepo = deckContentRepo;
    }
    #endregion

    #region Public class methods
    /// <summary>
    /// Gets DeckContents objects associated with the given deck ID.
    /// </summary>
    /// <param name="deckId">The ID of the deck to search.</param>
    /// <returns>A list of DeckContent objects that belong to the given deck ID.</returns>
    public List<DeckContent> GetDeckContentsByDeckId(long deckId) {
        return _deckContentRepo.GetDeckContentsByDeckId(deckId).ToList();
    }

    /// <summary>
    /// Calls the repository to save a list of deck contents to the database.
    /// </summary>
    /// <param name="deckContents">The deck contents that will be stored to the database.</param>
    /// <returns>A task to await.</returns>
    public Task SaveDeckContentAsync(IEnumerable<DeckContent> deckContents) {
        return _deckContentRepo.SaveDeckContentAsync(deckContents);
    }
    
    /// <summary>
    /// Calls the repository to delete all deck contents associated with a given deck ID..
    /// </summary>
    /// <param name="deckId">The deck ID to delete deck contents that have an association.</param>
    /// <returns>A task to await.</returns>
    public Task DeleteDeckContentsByDeckIdAsync(long deckId) {
        return _deckContentRepo.DeleteDeckContentsByDeckIdAsync(deckId);
    }
    #endregion

}