using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend.Services.Implementations;

/// <summary>
/// Implementation of the CardSetService. Contains business logic for retrieving and storing card set data.
/// </summary>
///
/// /// <remarks>See also: <see cref="Data.Models.CardSet"/></remarks>
public class CardSetServiceImpl : ICardSetService {

    #region Class members
    
    private readonly ICardSetRepo _cardSetRepo;
    
    #endregion
    
    #region Ctors
    
    /// <summary>
    /// Dependency injected constructor for CardSetServiceImpl class.
    /// </summary>
    /// <param name="cardSetRepo">The card set repository for connecting to the database.</param>
    public CardSetServiceImpl(ICardSetRepo cardSetRepo) {
        _cardSetRepo = cardSetRepo;
    }
    
    #endregion


    #region Public class methods
    /// <summary>
    /// Gets a card with the given card set code and rarity.
    /// </summary>
    /// <param name="cardSetCode">The code of the card in the set to search.</param>
    /// <param name="cardSetRarityCode">The rarity of the card in the set to search.</param>
    /// <returns>Retrieves all card sets that a card is in.</returns>
    public async Task<CardSet> GetCardSetByCodeAndRarityCodeAsync(string cardSetCode, string cardSetRarityCode) {
        return await _cardSetRepo.GetCardSetByCodeAndRarityCodeAsync(cardSetCode, cardSetRarityCode);
    }
    
    /// <summary>
    /// Gets a list of card sets for the given card ID.
    /// </summary>
    /// <param name="cardId">The id of the card to search.</param>
    /// <returns>Retrieves all card sets that a card is in.</returns>
    public List<CardSet> GetCardSetsByCardId(int cardId) {
        return _cardSetRepo.GetCardSetsByCardId(cardId).ToList();
    }
    #endregion


}