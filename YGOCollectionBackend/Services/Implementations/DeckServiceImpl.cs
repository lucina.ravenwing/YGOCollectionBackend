using System.Security.Claims;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend.Services.Implementations;

/// <summary>
/// Implementation of DeckService. Contains business logic for retrieving and storing card data pertaining to decks.
/// </summary>
///
/// <remarks>See also: <see cref="Data.Models.Deck"/></remarks>
public class DeckServiceImpl : IDeckService {
    #region Class members
    private readonly IDeckRepo _deckRepo;
    private readonly IDeckContentService _deckContentService;
    private readonly IUserService _userService;
    private readonly ICardSetService _cardSetService;
    private readonly ICardService _cardService;
    #endregion

    #region Ctors
    /// <summary>
    /// Dependency injected constructor for the DeckServiceImpl class.
    /// </summary>
    /// <param name="deckRepo">The deck repository for connecting to the database.</param>
    /// <param name="deckContentService">A service to handle all calls into the DeckContent repository.</param>
    /// <param name="userService">A service to handle all calls into the User repository.</param>
    /// <param name="cardSetService">A service to handle all calls into the CardSet repository.</param>
    /// <param name="cardService">A service to handle all calls into the CardSet repository.</param>
    public DeckServiceImpl(IDeckRepo deckRepo, IDeckContentService deckContentService, IUserService userService,
        ICardSetService cardSetService, ICardService cardService) {
        _deckRepo = deckRepo;
        _deckContentService = deckContentService;
        _userService = userService;
        _cardSetService = cardSetService;
        _cardService = cardService;
    }
    #endregion

    #region Public class methods
    /// <summary>
    /// Gets a user's decks based off the access token passed into the application.
    /// </summary>
    /// <param name="user">The information from the access token passed in the header.</param>
    /// <returns>The list of the decks the user has saved in the application.</returns>
    public async Task<List<DeckDataVm>> GetDecksAsync(ClaimsIdentity user) {
        // Get the user based off the ClaimsIdentity from the HTTP context.
        User appUser = await GetAppUser(user);

        List<Deck> decksList = new();
        List<DeckDataVm> deckVms = new();

        // If the user exists, then get their list of decks.
        if (appUser.Id != 0) {
            decksList = _deckRepo.GetDecksByUserId(appUser.Id).ToList();
        }

        // Loop through the decks returned, add them to a new list, then set the CardSet's Card attribute to null
        // to avoid infinite recursion (JSON return breaks).
        foreach (Deck deck in decksList) {
            List<DeckContent> deckContentsList = _deckContentService.GetDeckContentsByDeckId(deck.Id);

            // Could not force the card to not load its card sets since it's already in memory.
            foreach (DeckContent content in deckContentsList) {
                if (content.CardSet?.Card != null) {
                    content.CardSet.Card.CardSets = new List<CardSet>();
                }
            }

            deckVms.Add(new DeckDataVm(deck, deckContentsList));
        }

        return deckVms;
    }

    /// <summary>
    /// Saves a deck to the database using the supplied information.
    /// </summary>
    /// <param name="deck">The information about the deck to be saved to the database.</param>
    /// <param name="user">The user information to pull user id.</param>
    /// <returns>A tuple indicating the success or failure of the transaction.</returns>
    public async Task<Tuple<int, string>> SaveDeckAsync(SaveDeckVm deck, ClaimsIdentity user) {
        // Get the user based off the ClaimsIdentity from the HTTP context.
        User appUser = await GetAppUser(user);
        Deck savedDeck;

        // The ID will be 0 if the user doesn't exist.
        if (appUser.Id > 0) {
            List<DeckContent> deckContents = new();
            HashSet<short> positionSet = new();

            // Ensure a deck with the same name and owner doesn't already exist.
            if (!_deckRepo.ExistsByUserIdAndName(appUser.Id, deck.DeckName)) {
                savedDeck = await _deckRepo.SaveDeckAsync(new Deck {
                    DeckName = deck.DeckName,
                    UserId = appUser.Id
                });
            } else {
                return new Tuple<int, string>(400, "Error: Deck already exists.");
            }

            // If the deck was properly saved, then begin the steps to set up the DeckContents.
            if (savedDeck.Id > 0 && deck.DeckContents != null) {
                // Set up the last position for each deck.
                short lastPositionMain = 60;
                short lastPositionExtra = 75;
                short lastPositionSide = 90;

                // Check that the Card position fits into the proper range for the (valid) sub deck.
                foreach (SaveDeckContentsVm content in deck.DeckContents) {
                    content.Subdeck = char.ToUpper(content.Subdeck);

                    string validation = CheckSubDeckAndPosition(content.Position, content.Subdeck, ref lastPositionMain,
                        ref lastPositionExtra, ref lastPositionSide, positionSet);

                    string extraDeckValidation = await CheckExtraDeck(content.CardSetCode, 
                        content.CardSetRarityCode, content.Subdeck);

                    // The previous methods return a String that will start with Error if there are any issues.
                    // If this happens, the deck is deleted and a 400 error is returned.
                    if (validation.Contains("Error") || extraDeckValidation.Contains("Error")) {
                        await _deckRepo.DeleteDeckById(savedDeck.Id);

                        string returnString = validation.Contains("Error") ? validation : extraDeckValidation;
                        
                        return new Tuple<int, string>(400, returnString);
                    }

                    // If there were no errors in the validation, the methods return a position number as a string.
                    // This string is parsed, then added to a set that will be used for the validation steps.
                    content.Position = short.Parse(validation);
                    positionSet.Add(content.Position);

                    // If everything is valid, add the Card to the DeckContents List to be saved after the loop.
                    deckContents.Add(new DeckContent {
                        CardSetCode = content.CardSetCode,
                        CardSetRarityCode = content.CardSetRarityCode,
                        Position = content.Position,
                        DeckId = savedDeck.Id
                    });
                }

                // Save the DeckContents list.
                await _deckContentService.SaveDeckContentAsync(deckContents);
            }

            // If the deck was not saved, return an Internal Server Error.
            if (savedDeck.Id == 0) {
                return new Tuple<int, string>(500, "Error: Deck not saved for unknown reason.");
            }
        } else {
            // This should not happen as the user is passing in a cookie.
            return new Tuple<int, string>(500, "Error: User not able to be found in database.");
        }

        return new Tuple<int, string>(200, "Deck saved successfully. Deck ID: " + savedDeck.Id);
    }

    /// <summary>
    /// Updates a user's deck in the database with the given data.
    /// </summary>
    /// <param name="deck">The information about the deck and the contents to be updated.</param>
    /// <param name="user">The user to ensure the correct deck is being updated and the user has permissions.</param>
    /// <returns>A tuple indicating the success or failure of the transaction.</returns>
    public async Task<Tuple<int, string>> UpdateDeckAsync(UpdateDeckVm deck, ClaimsIdentity user) {
        // Get the user based off the ClaimsIdentity from the HTTP context.
        User appUser = await GetAppUser(user);

        //Check if the deck name exists for the user first, then do this. Also figure out how to update the deck contents.
        if (appUser.Id > 0) {
            List<DeckContent> deckContents = new();
            HashSet<short> positionSet = new();

            Deck savedDeck = await _deckRepo.GetDeckByDeckIdAsync(deck.DeckId);

            // If the deck didn't properly save, it will return 0 for the UserId and a 400 error will be returned.
            if (savedDeck.UserId == 0) {
                return new Tuple<int, string>(400, "Error: Deck does not exist in the database.");
            }

            // If the user does not own the deck, return a 403.
            if (savedDeck.UserId != appUser.Id) {
                return new Tuple<int, string>(403, "Error: Deck does not belong to given user.");
            }

            // Update the deck name to the new value passed in.
            savedDeck.DeckName = deck.DeckName;

            // Delete all deck contents as they will be added again.
            await _deckContentService.DeleteDeckContentsByDeckIdAsync(deck.DeckId);
            
            if (deck.DeckContents != null) {
                // Set the last position of each deck subtype.
                short lastPositionMain = 60;
                short lastPositionExtra = 75;
                short lastPositionSide = 90;

                // Check that the Card position fits into the proper range for the (valid) sub deck.
                foreach (UpdateDeckContentsVm content in deck.DeckContents) {
                    content.Subdeck = char.ToUpper(content.Subdeck);

                    string validation = CheckSubDeckAndPosition(content.Position, content.Subdeck, ref lastPositionMain,
                        ref lastPositionExtra, ref lastPositionSide, positionSet);
                    
                    string extraDeckValidation = await CheckExtraDeck(content.CardSetCode, 
                        content.CardSetRarityCode, content.Subdeck);

                    // The previous methods return a String that will start with Error if there are any issues.
                    // If this happens, the deck is deleted and a 400 error is returned.                    
                    if (validation.Contains("Error") || extraDeckValidation.Contains("Error")) {
                        string returnString = validation.Contains("Error") ? validation : extraDeckValidation;
                        
                        return new Tuple<int, string>(400, returnString);
                    }

                    // If there were no errors in the validation, the methods return a position number as a string.
                    // This string is parsed, then added to a set that will be used for the validation steps.
                    content.Position = short.Parse(validation);
                    positionSet.Add(content.Position);

                    // If everything is valid, add the Card to the DeckContents List to be saved after the loop.
                    deckContents.Add(new DeckContent {
                        CardSetCode = content.CardSetCode,
                        CardSetRarityCode = content.CardSetRarityCode,
                        Position = content.Position,
                        DeckId = savedDeck.Id
                    });
                }

                // Save the updated deck to the database.
                Deck updatedDeck = await _deckRepo.UpdateDeck(savedDeck);

                // If the deck doesn't save for some reason, throw a 500 error.
                if (updatedDeck.UserId == 0) {
                    return new Tuple<int, string>(500, "Error: Deck not updated.");
                }

                // Save the DeckContents list.
                await _deckContentService.SaveDeckContentAsync(deckContents);
            }
        } else {
            // This shouldn't happen since the user is getting passed in via cookie.
            return new Tuple<int, string>(500, "Error: User not able to be found in database.");
        }

        return new Tuple<int, string>(200, "Deck updated successfully.");
    }

    /// <summary>
    /// Deletes a deck by the given ID. Ensures the user is the owner.
    /// </summary>
    /// <param name="deckId">The ID of the deck to delete.</param>
    /// <param name="user">The user to ensure the correct deck is being updated and the user has permissions.</param>
    /// <returns>A tuple indicating the success or failure of the transaction.</returns>
    public async Task<Tuple<int, string>> DeleteDeckAsync(long deckId, ClaimsIdentity user) {
        // Get the user based off the ClaimsIdentity from the HTTP context.
        User appUser = await GetAppUser(user);

        // The userId will be 0 if they are not found.
        if (appUser.Id > 0) {
            // Get the deck based on the id.
            Deck savedDeck = await _deckRepo.GetDeckByDeckIdAsync(deckId);

            // If the user owns the deck, delete it, otherwise throw a 403 error.
            if (savedDeck.UserId == appUser.Id) {
                await _deckContentService.DeleteDeckContentsByDeckIdAsync(savedDeck.Id);
                await _deckRepo.DeleteDeckById(deckId);
            } else {
                return new Tuple<int, string>(403, "Error: Given user doesn't own the provided deck.");
            }

            return new Tuple<int, string>(200, "Deck deleted successfully.");
        }

        return new Tuple<int, string>(500, "Error: User not able to be found in database.");
    }
    #endregion

    #region Private class methods
    /// <summary>
    /// Pull the user from the database based on the ClaimsIdentity from the HTTP Context.
    /// </summary>
    /// <param name="user">The user to search.</param>
    /// <returns>The user from the database, or a blank user with an ID of 0 if not found.</returns>
    private async Task<User> GetAppUser(ClaimsIdentity user) {
        string? username = user.FindFirst(ClaimTypes.Name)?.Value;

        User appUser = new();
        
        if (username != null) {
            return await _userService.GetUserByUsernameAsync(username);
        }

        return appUser;
    }

    /// <summary>
    /// Validates the subdeck of the Card and ensures a valid position is set. Will return an error string if invalid.
    /// </summary>
    /// <param name="position">The position of the card that was passed in.</param>
    /// <param name="subdeck">The subdeck of the card that was passed in.</param>
    /// <param name="main">The max position of the main deck.</param>
    /// <param name="extra">The max position of the extra deck.</param>
    /// <param name="side">The max position of the side deck.</param>
    /// <param name="positionSet">A set containing all the positions that have already been used.</param>
    /// <returns>A string with either the position number or an error explaining the invalid input.</returns>
    private static string CheckSubDeckAndPosition(short position, char? subdeck, ref short main, ref short extra, ref short side,
        IReadOnlySet<short> positionSet) {
        
        // The only valid subdeck values are M(ain), E(xtra), and S(ide).
        if (subdeck == null || (subdeck != 'M' && subdeck != 'E' && subdeck != 'S')) {
            return "Error: Not a valid sub deck value. Only M, E, and S are acceptable.";
        }

        // Loop through position numbers until a valid position is found if the value isn't valid right away.
        while (positionSet.Contains(position) || (subdeck == 'M' && position is > 60 or < 1) ||
               (subdeck == 'E' && position is > 75 or < 61) ||
               (subdeck == 'S' && position is > 90 or < 76)) {
            switch (subdeck) {
                // If the card is in the M(ain), set the position to the max value, then count down. If the number goes
                // below 1, then there are too many cards in the main deck as that would mean there are 60. Repeat for
                // E(xtra) and S(ide), but with 15 cards for each.
                case 'M':
                    position = main--;

                    if (position == 0) {
                        return "Error: Too many cards in the main deck.";
                    }

                    break;
                case 'E':
                    position = extra--;

                    if (position == 60) {
                        return "Error: Too many cards in the extra deck.";
                    }

                    break;
                case 'S':
                    position = side--;

                    if (position == 75) {
                        return "Error: Too many cards in the side deck.";
                    }

                    break;
            }
        }

        return position.ToString();
    }

    /// <summary>
    /// Checks whether there is an extra deck card in the main deck or a main deck card in the extra deck.
    /// </summary>
    /// <param name="cardSetCode">The code of the card to check.</param>
    /// <param name="cardSetRarityCode">The rarity of the card to check.</param>
    /// <param name="subdeck">The subdeck of the card. M, S, or E. Will be used to check if the card is valid.</param>
    /// <returns></returns>
    private async Task<string> CheckExtraDeck(string cardSetCode, string cardSetRarityCode, char subdeck) {
        CardSet cardSet =
            await _cardSetService.GetCardSetByCodeAndRarityCodeAsync(cardSetCode,
                cardSetRarityCode);
        Card card = await _cardService.GetCardByIdAsync(cardSet.CardId);

        // Check whether the card belongs in the extra deck and whether it is or is not there.
        if (card.CardType != null) {
            switch (subdeck) {
                case 'M' when (card.CardType.Contains("Fusion") ||
                                      card.CardType.Contains("Synchro") ||
                                      card.CardType.Contains("XYZ") ||
                                      card.CardType.Contains("Link")):
                    return "Error: There is an extra deck monster in the main or side deck.";
                case 'E' when !card.CardType.Contains("Fusion") &&
                              !card.CardType.Contains("Synchro") && 
                              !card.CardType.Contains("XYZ") &&
                              !card.CardType.Contains("Link"):
                    return "Error: There is a non-extra deck card in the extra deck.";
            }
        } else {
            return "Error: Card type is null.";
        }

        return "";
    }
    #endregion
}