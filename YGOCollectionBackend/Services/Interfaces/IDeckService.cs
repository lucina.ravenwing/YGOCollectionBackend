using System.Security.Claims;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;

namespace YGOCollectionBackend.Services.Interfaces; 

/// <summary>
/// Interface for service that completes tasks related to deck building.
/// </summary>
///
/// <remarks>See also: <see cref="Data.Models.Deck"/></remarks>
public interface IDeckService {
    /// <summary>
    /// Gets a user's decks based off the access token passed into the application.
    /// </summary>
    /// <param name="user">The information from the access token passed in the header.</param>
    /// <returns>The list of the decks the user has saved in the application.</returns>
    Task<List<DeckDataVm>> GetDecksAsync(ClaimsIdentity user);

    /// <summary>
    /// Saves a deck to the database using the supplied information.
    /// </summary>
    /// <param name="deck">The information about the deck to be saved to the database.</param>
    /// <param name="user">The user information to pull user id.</param>
    /// <returns>A tuple indicating the success or failure of the transaction.</returns>
    Task<Tuple<int, string>> SaveDeckAsync(SaveDeckVm deck, ClaimsIdentity user);

    /// <summary>
    /// Updates a user's deck in the database with the given data.
    /// </summary>
    /// <param name="deck">The information about the deck and the contents to be updated.</param>
    /// <param name="user">The user to ensure the correct deck is being updated and the user has permissions.</param>
    /// <returns>A tuple indicating the success or failure of the transaction.</returns>
    Task<Tuple<int, string>> UpdateDeckAsync(UpdateDeckVm deck, ClaimsIdentity user);

    /// <summary>
    /// Deletes a deck by the given ID. Ensures the user is the owner.
    /// </summary>
    /// <param name="deckId">The ID of the deck to delete.</param>
    /// <param name="user">The user to ensure the correct deck is being updated and the user has permissions.</param>
    /// <returns>A tuple indicating the success or failure of the transaction.</returns>
    Task<Tuple<int, string>> DeleteDeckAsync(long deckId, ClaimsIdentity user);
}