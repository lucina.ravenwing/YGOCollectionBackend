using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Services.Interfaces;

/// <summary>
/// Interface for service that completes tasks related to deck building.
/// </summary>
///
/// See also: <see cref="Data.Models.DeckContent"/>
public interface IDeckContentService {

    /// <summary>
    /// Gets DeckContents objects associated with the given deck ID.
    /// </summary>
    /// <param name="deckId">The ID of the deck to search.</param>
    /// <returns>A list of DeckContent objects that belong to the given deck ID.</returns>
    List<DeckContent> GetDeckContentsByDeckId(long deckId);

    /// <summary>
    /// Calls the repository to save a list of deck contents to the database.
    /// </summary>
    /// <param name="deckContents">The deck contents that will be stored to the database.</param>
    /// <returns>A task to await.</returns>
    Task SaveDeckContentAsync(IEnumerable<DeckContent> deckContents);
    
    /// <summary>
    /// Calls the repository to delete all deck contents associated with a given deck ID..
    /// </summary>
    /// <param name="deckId">The deck ID to delete deck contents that have an association.</param>
    /// <returns>A task to await.</returns>
    Task DeleteDeckContentsByDeckIdAsync(long deckId);

}