using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Services.Interfaces;

/// <summary>
/// Interface for service that completes tasks related to card sets.
/// </summary>
///
/// <remarks>See also: <see cref="Data.Models.CardSet"/></remarks>
public interface ICardSetService {
        
    /// <summary>
    /// Gets a list of card sets for the given card ID.
    /// </summary>
    /// <param name="cardId">The id of the card to search.</param>
    /// <returns>Retrieves all card sets that a card is in.</returns>
    List<CardSet> GetCardSetsByCardId(int cardId);
    
    /// <summary>
    /// Gets a card with the given card set code and rarity.
    /// </summary>
    /// <param name="cardSetCode">The code of the card in the set to search.</param>
    /// <param name="cardSetRarityCode">The rarity of the card in the set to search.</param>
    /// <returns>Retrieves all card sets that a card is in.</returns>
    Task<CardSet> GetCardSetByCodeAndRarityCodeAsync(string cardSetCode, string cardSetRarityCode);
}