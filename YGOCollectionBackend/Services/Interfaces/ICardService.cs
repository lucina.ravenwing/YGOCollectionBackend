using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Services.Interfaces;

/// <summary>
/// Interface for service that completes tasks related to cards.
/// </summary>
///
/// <remarks>See also: <see cref="Data.Models.Card"/></remarks>
public interface ICardService {

    /// <summary>
    /// Get the card with the given ID from the repository.
    /// </summary>
    /// <param name="cardId">The ID of the card to search.</param>
    /// <returns>The card with the given ID or a blank card if one is not found.</returns>
    Task<Card> GetCardByIdAsync(long cardId);

}