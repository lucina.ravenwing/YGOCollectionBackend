using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;

namespace YGOCollectionBackend.Services.Interfaces; 

/// <summary>
/// Interface for AuthService implementations.
/// </summary>
public interface IAuthService {
    
    /// <summary>
    /// Signs a user up for the application.
    /// </summary>
    /// <param name="signUpRequest">Request with information needed to create a new user.</param>
    /// <returns>Details about the status of the user creation.</returns>
    Task<Tuple<int, string>> SignUpAsync(SignUpRequestVm signUpRequest);
    
    /// <summary>
    /// Logs a user into the application.
    /// </summary>
    /// <param name="logInRequestVm">Request with information needed to a log a user in.</param>
    /// <returns>Details about the status of the log in attempt.</returns>
    Task<Tuple<int, LogInResponse>> LogInAsync(LogInRequestVm logInRequestVm);

    /// <summary>
    /// Logs a user out of the application.
    /// </summary>
    /// <param name="refreshToken">Request with the RefreshToken associated with the user to log out.</param>
    /// <returns>A tuple of the response code and response string.</returns>
    Task<Tuple<int, string>> LogOutAsync(string refreshToken);

    /// <summary>
    /// Resends confirmation email to a user that has already registered.
    /// </summary>
    /// <param name="email">The email of the user asking for a new confirmation email.</param>
    /// <returns>A tuple of response code and message to be used in the return to the front end.</returns>
    Task<Tuple<int, string>> ResendConfirmationEmailAsync(string email);

    /// <summary>
    /// Sends email allowing a user to reset their password.
    /// </summary>
    /// <param name="forgotPasswordVm">The request received from the user containing their username or email.</param>
    /// <returns>A tuple of the response code and message to be returned to the front end.</returns>
    Task<Tuple<int, string>> ForgotPasswordAsync(ForgotPasswordVm forgotPasswordVm);

    /// <summary>
    /// Resets a users password on the auth server.
    /// </summary>
    /// <param name="resetPasswordVm">The request view model that contains the token, the user's email, the new password,
    /// and the origin on the request.</param>
    /// <returns>A tuple of the response code and message to be returned to the front end.</returns>
    Task<Tuple<int, string>> ResetPasswordAsync(ResetPasswordVm resetPasswordVm);

    /// <summary>
    /// Refreshes a user's access token through the auth server.
    /// </summary>
    /// <param name="tokenRefreshVm">The refresh request containing the current token and the refresh token.</param>
    /// <returns>A tuple of the response code and message to be returned to the front end.</returns>
    Task<Tuple<int, LogInResponse>> TokenRefreshAsync(TokenRefreshVm tokenRefreshVm);
}