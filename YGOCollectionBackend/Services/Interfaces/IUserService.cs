using YGOCollectionBackend.Data.Models;

namespace YGOCollectionBackend.Services.Interfaces;

/// <summary>
/// Interface for service that completes tasks related to users of the application
/// </summary>
///
/// <remarks>See also: <see cref="Data.Models.User"/></remarks>
public interface IUserService {

    /// <summary>
    /// Gets the user of the application that has the given username from the repository.
    /// </summary>
    /// <param name="username">The username to search.</param>
    /// <returns>The user with the given username or a blank user if one is not found.</returns>
    Task<User> GetUserByUsernameAsync(string username);
    
    /// <summary>
    /// Gets the user of the application that has the given email from the repository.
    /// </summary>
    /// <param name="email">The email to search.</param>
    /// <returns>The user with the given email or a blank user if one is not found.</returns>
    Task<User> GetUserByEmailAsync(string email);
    
    /// <summary>
    /// Sends information to the User repository to store a new User in the database.
    /// </summary>
    /// <param name="newUser">A User object that will be stored into the database.</param>
    /// <returns>The user that was created in the database or a blank User if it failed to create.</returns>
    Task<User> CreateUserAsync(User newUser);

}