pipeline {
    agent { label 'ygo-coll-backend' }
    stages {
        stage('build') {
            steps {
                updateGitlabCommitStatus name: 'build', state: 'running'
                
                echo 'Starting build stage.'
                
                sh 'cp /home/jenkins/appsettings.json ./YGOCollectionBackend; cp /home/jenkins/appsettings.Development.json ./YGOCollectionBackend'
                sh 'cp /home/jenkins/nlog.config ./YGOCollectionBackend'
                sh 'dotnet build' 
                
                updateGitlabCommitStatus name: 'build', state: 'success'
            }
        }
        stage('test') {
            steps {
                updateGitlabCommitStatus name: 'test', state: 'running'
            
                echo 'Starting test stage.'
            
                sh 'dotnet test ./YGOCollectionBackend_UnitTests/bin/Debug/net7.0/YGOCollectionBackend_UnitTests.dll'
            
                updateGitlabCommitStatus name: 'test', state: 'success'
            }
        }
        stage('release') {
            steps {
                updateGitlabCommitStatus name: 'release', state: 'running'
                
                echo 'Starting release stage. Includes:\n\t- Archive the old release folder.\n\t- Publish the new release folder. \n\t- Build the docker image locally.\n\t- Push the image to Docker Hub.'
                                
                sh 'dotnet publish ./YGOCollectionBackend/YGOCollectionBackend.csproj -c RELEASE -o ./Release'
                
                // Archive the old release folder and move new folder to the correct location. mv command ends with or true in case the original folder doesn't exist.
                sh 'mv /home/jenkins/deploy-asp/* /home/jenkins/archived-asp || true'
                sh 'cp -r ./YGOCollectionBackend/bin/Release/* /home/jenkins/deploy-asp || true'
                
                // Docker deploy steps. Both have an or true in case the docker containers on the server get cleaned out.
                sh 'docker stop ygo-collection-backend || true'
                sh 'docker rm ygo-collection-backend || true'
                
                // Push image to DockerHub
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'jenkins-dockerhub') {
                        app = docker.build("lucinaravenwing/ygo-collection-backend")
                        app.push("${env.BUILD_NUMBER}")
                        app.push("latest")
                    }
                }
                
                updateGitlabCommitStatus name: 'release', state: 'success'
            }
        }
        stage('deploy') {
            steps {
                updateGitlabCommitStatus name: 'deploy', state: 'success'
                
                echo 'Starting deploy stage. Includes:\n\t- Running the docker container.\n\t- (WIP) Testing the deploy worked.'
                
                sh 'docker run -d -it --network=host --name=ygo-collection-backend -v logs:/logs lucinaravenwing/ygo-collection-backend'
                
                updateGitlabCommitStatus name: 'deploy', state: 'success'
            }
        }
    }
}