# YuGiOh Collection Backend (WIP)

[![Build Status](https://jenkins.lucinaravenwing.net/buildStatus/icon?job=ygo-coll-backend&style=plastic)](https://jenkins.lucinaravenwing.net/job/ygo-coll-backend/)

## Repository link
https://gitlab.com/lucina.ravenwing/YGOCollectionBackend

## URL
https://www.ygo.lucinaravenwing.net

## Description
The backend for a Yu-Gi-Oh collection app. The application allows users to compile a list of cards that they own from the Yu-Gi-Oh trading card game and view the approximate worth of their collection. They are also able to build decks that they would like to put together and pull from their collection to approximate how much the deck will cost to build or complete. If the user is logged in, they can save the collection and decks, but are also able to do both activities as a guest, but are unable to save their work.</br>
The back end provides an API that the front end will call into. The API exposes authentication endpoints as well as endpoints that 
return information about cards, a user's collection, saved decks, and wishlist. The back end is written in C# using ASP.NET.

## Languages
C#

## Resources Used
ASP.NET\
Entity Framework\
CI/CD pipeline\
Docker\
nginx

==================
