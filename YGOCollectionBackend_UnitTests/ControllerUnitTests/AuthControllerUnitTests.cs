using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using YGOCollectionBackend.Controllers;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend_UnitTests.ControllerUnitTests; 

public class AuthControllerUnitTests {

    [Fact]
    public async void SignUpWithErrorReturnsBadRequest() {
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(authService => authService.SignUpAsync(It.IsAny<SignUpRequestVm>()))
            .ReturnsAsync(new Tuple<int, string>(400, "Error"));
        
        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        IActionResult response = await authController.SignUp(new SignUpRequestVm());
        Assert.IsType<BadRequestObjectResult>(response);
        
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;
        Assert.Contains("Error", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void SignUpWithNoErrorsReturnsOkResponse() {
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(authService => authService.SignUpAsync(It.IsAny<SignUpRequestVm>()))
            .ReturnsAsync(new Tuple<int, string>(200, "Good!"));
        
        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        IActionResult response = await authController.SignUp(new SignUpRequestVm());
        Assert.IsType<OkObjectResult>(response);
        
        OkObjectResult? responseConvert = response as OkObjectResult;
        Assert.Contains("Good!", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void LogInWithIncorrectCredentialsReturns400Error() {
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(authService => authService.LogInAsync(It.IsAny<LogInRequestVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(400, new LogInResponse(){Message = "Error: Username, email, or password invalid."}));


        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        IActionResult response = await authController.LogIn(new LogInRequestVm());
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: Username", (responseConvert?.Value as LogInResponse)?.Message);
    }

    [Fact]
    public async void LogInWithUnconfirmedUserReturnsRedirect() {
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(authService => authService.LogInAsync(It.IsAny<LogInRequestVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(401, new LogInResponse(){Message = "Error: Email has not been confirmed for this account."}));


        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        IActionResult response = await authController.LogIn(new LogInRequestVm());
        UnauthorizedObjectResult? responseConvert = response as UnauthorizedObjectResult;

        Assert.IsType<UnauthorizedObjectResult>(response);
        Assert.Contains("not been confirmed for this account.", (responseConvert?.Value as LogInResponse)?.Message);

    }

    [Fact]
    public async void LogInWithProperInfoAndValidatedUserReturnsExpectedResponse() {
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(authService => authService.LogInAsync(It.IsAny<LogInRequestVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(200, new LogInResponse() {
                Message = "\"Token created successfully!\"",
                Token = "\"this-is-a-token\"",
                RefreshToken = "\"this-is-a-refresh-token\"",
                ExpiresAt = "\"2023-05-03\""
            }));

        Mock<IResponseCookies> mockCookies = new();
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };

        IActionResult response = await authController.LogIn(new LogInRequestVm());
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("\"Token created successfully!\"", (responseConvert?.Value as LogInResponse)?.Message);
        Assert.Contains("\"this-is-a-token\"", (responseConvert?.Value as LogInResponse)?.Token);
        Assert.Contains("\"this-is-a-refresh-token\"", (responseConvert?.Value as LogInResponse)?.RefreshToken);
        Assert.Contains("\"2023-05-03\"", (responseConvert?.Value as LogInResponse)?.ExpiresAt);
    }

    [Fact]
    public async void LogOutWithBlankRefreshTokenReturnsBadRequest() {
        Mock<IAuthService> mockAuthService = new();
        
        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();

        string? s = null;

        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Request-Token"]).Returns(s);

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };

        IActionResult response = await authController.LogOut(); 
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
        Assert.Equal("Error: Token not present in cookies.", responseConvert?.Value);
    }

    [Fact]
    public async void LogOutWith400ReturnFromAuthServerReturnsProperStatus() {
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(authService => authService.LogOutAsync(It.IsAny<string>()))
            .ReturnsAsync(new Tuple<int, string>(400, "Invalid input."));
        
        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();
    
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Refresh-Token"]).Returns("this-is-a-refresh-token");
    
        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };
        
        IActionResult response = await authController.LogOut();
        ObjectResult? responseConvert = response as ObjectResult;
    
        Assert.IsType<ObjectResult>(response);
        Assert.Equal(400, responseConvert?.StatusCode);
        Assert.Equal("Invalid input.", responseConvert?.Value);
    }

    [Fact]
    public async void LogOutWithProperDataReturnsProperResponse() {
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(authService => authService.LogOutAsync(It.IsAny<string>()))
            .ReturnsAsync(new Tuple<int, string>(200, "User logged out successfully."));
        
        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();
        
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Refresh-Token"]).Returns("this-is-a-refresh-token");
    
        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };
    
        IActionResult response = await authController.LogOut();
        ObjectResult? responseConvert = response as ObjectResult;
    
        Assert.IsType<ObjectResult>(response);
        Assert.Equal(200, responseConvert?.StatusCode);
        Assert.Equal("User logged out successfully.", responseConvert?.Value);
    }

    [Fact]
    public async void ResendConfirmationEmailWithBlankEmailReturns400() {
        ResendConfirmationEmailVm emailConfirmation = new() {
            EmailAddress = ""
        };

        AuthController authController = new(new Mock<IAuthService>().Object, new Mock<IConfiguration>().Object);

        IActionResult response = await authController.ResendConfirmationEmail(emailConfirmation);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Equal(400, responseConvert?.StatusCode);
        Assert.Equal("Error: Email required.", responseConvert?.Value);
    }

    [Fact]
    public async void ResendConfirmationEmailWithUserAlreadyExistingReturns400() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ResendConfirmationEmailAsync(It.IsAny<string>()))
        .ReturnsAsync(new Tuple<int, string>(400, "Error: User already confirmed."));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        ResendConfirmationEmailVm emailConfirmation = new() {
            EmailAddress = "already.exists@gmail.com"
        };

        IActionResult response = await authController.ResendConfirmationEmail(emailConfirmation);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Equal(400, responseConvert?.StatusCode);
        Assert.Equal("Error: User already confirmed.", responseConvert?.Value);
    }

    [Fact]
    public async void ResendConfirmationEmailWithInternalServerErrorReturns500() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ResendConfirmationEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new Tuple<int, string>(500, "I don't know, some error or something."));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        ResendConfirmationEmailVm emailConfirmation = new() {
            EmailAddress = "internalServerError@gmail.com"
        };

        IActionResult response = await authController.ResendConfirmationEmail(emailConfirmation);
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
        Assert.Equal("I don't know, some error or something.", responseConvert?.Value);
    }

    [Fact]
    public async void ResendConfirmationEmailWithProperInputsReturns200() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ResendConfirmationEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new Tuple<int, string>(200, "It worked! :D"));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        ResendConfirmationEmailVm emailConfirmation = new() {
            EmailAddress = "itWorked@gmail.com"
        };

        IActionResult response = await authController.ResendConfirmationEmail(emailConfirmation);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Equal(200, responseConvert?.StatusCode);
        Assert.Equal("It worked! :D", responseConvert?.Value);
    }
    
    [Fact]
    public async void ForgotPasswordWithNoUsernameOrEmailReturns400() {
        Mock<IAuthService> mockAuthService = new();

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        ForgotPasswordVm forgotPassword = new() {
            UsernameEmail = ""
        };

        IActionResult response = await authController.ForgotPassword(forgotPassword);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Equal(400, responseConvert?.StatusCode);
        Assert.Equal("Please enter a username or email.", responseConvert?.Value);
    }

    [Fact]
    public async void ForgotPasswordWithImproperInputReturns400() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ForgotPasswordAsync(It.IsAny<ForgotPasswordVm>()))
            .ReturnsAsync(new Tuple<int, string>(400, "Error: Improper user input."));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        ForgotPasswordVm forgotPassword = new() {
            UsernameEmail = "lucinaravenwing.net@gmail.com"
        };

        IActionResult response = await authController.ForgotPassword(forgotPassword);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Equal(400, responseConvert?.StatusCode);
        Assert.Equal("Error: Improper user input.", responseConvert?.Value);
    }
    
    [Fact]
    public async void ForgotPasswordWith500ErrorReturns500() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ForgotPasswordAsync(It.IsAny<ForgotPasswordVm>()))
            .ReturnsAsync(new Tuple<int, string>(500, "Error: Email not sent."));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        ForgotPasswordVm forgotPassword = new() {
            UsernameEmail = "lucinaravenwing.net@gmail.com"
        };

        IActionResult response = await authController.ForgotPassword(forgotPassword);
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
        Assert.Equal("Error: Email not sent.", responseConvert?.Value);
    }
    
    [Fact]
    public async void ForgotPasswordWithProperInputsReturns200() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ForgotPasswordAsync(It.IsAny<ForgotPasswordVm>()))
            .ReturnsAsync(new Tuple<int, string>(200, "Reset password email sent successfully."));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        ForgotPasswordVm forgotPassword = new() {
            UsernameEmail = "lucinaravenwing.net@gmail.com"
        };

        IActionResult response = await authController.ForgotPassword(forgotPassword);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Equal(200, responseConvert?.StatusCode);
        Assert.Equal("Reset password email sent successfully.", responseConvert?.Value);
    }

    [Fact]
    public async void ResetPasswordWith400ResponseReturns400() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ResetPasswordAsync(It.IsAny<ResetPasswordVm>()))
            .ReturnsAsync(new Tuple<int, string>(400, "Error: Password not able to be reset."));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        ResetPasswordVm resetPassword = new() {
            Token = "This-is-a-token",
            NewPassword = "DontUseThisPassword",
            Email = "lucinaravenwing.net@gmail.com",
            Origin = "https://ygo.lucinaravenwing.net"
        };

        IActionResult response = await authController.ResetPassword(resetPassword);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Equal(400, responseConvert?.StatusCode);
        Assert.Equal("Error: Password not able to be reset.", responseConvert?.Value);
    }
    
    [Fact]
    public async void ResetPasswordWith500ResponseReturns00() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ResetPasswordAsync(It.IsAny<ResetPasswordVm>()))
            .ReturnsAsync(new Tuple<int, string>(500, "Error: Password not able to be reset."));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        ResetPasswordVm resetPassword = new() {
            Token = "This-is-a-token",
            NewPassword = "DontUseThisPassword",
            Email = "lucinaravenwing.net@gmail.com",
            Origin = "https://ygo.lucinaravenwing.net"
        };

        IActionResult response = await authController.ResetPassword(resetPassword); 
        StatusCodeResult? responseConvert = response as StatusCodeResult;

        Assert.IsType<StatusCodeResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
    }

    [Fact]
    public async void ResetPasswordWithProperInputReturnsOkResponse() {
        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(authService => authService.ResetPasswordAsync(It.IsAny<ResetPasswordVm>()))
            .ReturnsAsync(new Tuple<int, string>(200, "Password successfully updated."));

        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);

        ResetPasswordVm resetPassword = new() {
            Token = "This-is-a-token",
            NewPassword = "DontUseThisPassword",
            Email = "lucinaravenwing.net@gmail.com",
            Origin = "https://ygo.lucinaravenwing.net"
        };

        IActionResult response = await authController.ResetPassword(resetPassword);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Equal(200, responseConvert?.StatusCode);
        Assert.Equal("Password successfully updated.", responseConvert?.Value);
    }
    
    [Fact]
    public async void TokenRefreshWith400ResponseReturns400() {
        Mock<IAuthService> mockAuthService = new();

        LogInResponse logInResponse = new() {
            Message = "Token was not able to be refreshed."
        };

        mockAuthService.Setup(authService => authService.TokenRefreshAsync(It.IsAny<TokenRefreshVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(400, logInResponse));

        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();
        
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Access-Token"]).Returns("this-is-an-access-token");
        mockContext.Setup(context => context.Request.Cookies["X-Refresh-Token"]).Returns("this-is-a-refresh-token");
    
        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };


        IActionResult response = await authController.TokenRefresh();
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Equal(400, responseConvert?.StatusCode);
        Assert.Equal("Token was not able to be refreshed.", (responseConvert?.Value as LogInResponse)?.Message);
    }
    
    [Fact]
    public async void TokenRefreshWithNullRefreshTokenReturns500() {
        Mock<IAuthService> mockAuthService = new();
        
        LogInResponse logInResponse = new() {
            Message ="Error: Cookie does not contain required information."
        };

        mockAuthService.Setup(authService => authService.TokenRefreshAsync(It.IsAny<TokenRefreshVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(500, logInResponse));

        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();

        string? s = null;
        
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Access-Token"]).Returns("NotNull");
        mockContext.Setup(context => context.Request.Cookies["X-Refresh-Token"]).Returns(s);
    
        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };

        IActionResult response = await authController.TokenRefresh();
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
    }
    
    [Fact]
    public async void TokenRefreshWithNullAccessTokenAndNoSectionValueReturns500() {
        Mock<IAuthService> mockAuthService = new();
        
        LogInResponse logInResponse = new() {
            Message ="Error: Cookie does not contain required information."
        };

        mockAuthService.Setup(authService => authService.TokenRefreshAsync(It.IsAny<TokenRefreshVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(500, logInResponse));

        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();

        string? s = null;
        
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Access-Token"]).Returns(s);
        mockContext.Setup(context => context.Request.Cookies["X-Refresh-Token"]).Returns("NotNull");
        
        Mock<IConfiguration> mockConfiguration = new();
        Mock<IConfigurationSection> mockJwtSection = new();
        // ReSharper disable once StringLiteralTypo
        mockJwtSection.Setup(section => section.Value);
        mockConfiguration.Setup(config => config.GetSection(It.IsAny<string>()))
            .Returns(mockJwtSection.Object);
    
        AuthController authController = new(mockAuthService.Object, mockConfiguration.Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };

        IActionResult response = await authController.TokenRefresh();
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
    }
    
    [Fact]
    public async void TokenRefreshWith500ResponseReturns500() {
        Mock<IAuthService> mockAuthService = new();
        
        LogInResponse logInResponse = new() {
            Message = "Token was not able to be refreshed."
        };

        mockAuthService.Setup(authService => authService.TokenRefreshAsync(It.IsAny<TokenRefreshVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(500, logInResponse));

        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();
        
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Access-Token"]).Returns("this-is-an-access-token");
        mockContext.Setup(context => context.Request.Cookies["X-Refresh-Token"]).Returns("this-is-a-refresh-token");
    
        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };

        IActionResult response = await authController.TokenRefresh();
        StatusCodeResult? responseConvert = response as StatusCodeResult;

        Assert.IsType<StatusCodeResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
    }
    
    [Fact]
    public async void TokenRefreshWithNullAccessTokenAndAValidSectionValueReturns200() {
        Mock<IAuthService> mockAuthService = new();
        
        LogInResponse logInResponse = new() {
            Message = "Token refreshed successfully."
        };

        mockAuthService.Setup(authService => authService.TokenRefreshAsync(It.IsAny<TokenRefreshVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(200, logInResponse));

        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();

        string? s = null;
        
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Access-Token"]).Returns(s);
        mockContext.Setup(context => context.Request.Cookies["X-Refresh-Token"]).Returns("NotNull");
        
        Mock<IConfiguration> mockConfiguration = new();
        Mock<IConfigurationSection> mockJwtSection = new();
        // ReSharper disable once StringLiteralTypo
        mockJwtSection.Setup(section => section.Value).Returns("27657D6734FBCAF23BF3DD8D7F217");
        mockConfiguration.Setup(config => config.GetSection(It.IsAny<string>()))
            .Returns(mockJwtSection.Object);
    
        AuthController authController = new(mockAuthService.Object, mockConfiguration.Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };

        IActionResult response = await authController.TokenRefresh();
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Equal(200, responseConvert?.StatusCode);
    }

    [Fact]
    public async void TokenRefreshWithProperInputReturnsOkResponse() {
        Mock<IAuthService> mockAuthService = new();

        LogInResponse logInResponse = new() {
            Message = "Token refreshed successfully."
        };

        mockAuthService.Setup(authService => authService.TokenRefreshAsync(It.IsAny<TokenRefreshVm>()))
            .ReturnsAsync(new Tuple<int, LogInResponse>(200, logInResponse));

        Mock<IResponseCookies> mockCookies = new();
        mockCookies.Setup(cookie => cookie.Delete(It.IsAny<string>())).Verifiable();
        
        Mock<HttpContext> mockContext = new();
        mockContext.Setup(context => context.Response.Cookies).Returns(mockCookies.Object);
        mockContext.Setup(context => context.Request.Cookies["X-Access-Token"]).Returns("this-is-an-access-token");
        mockContext.Setup(context => context.Request.Cookies["X-Refresh-Token"]).Returns("this-is-a-refresh-token");
    
        AuthController authController = new(mockAuthService.Object, new Mock<IConfiguration>().Object);
        
        authController.ControllerContext = new ControllerContext {
            HttpContext = mockContext.Object
        };

        IActionResult response = await authController.TokenRefresh();
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Equal(200, responseConvert?.StatusCode);
        Assert.Equal("Token refreshed successfully.", (responseConvert?.Value as LogInResponse)?.Message);
    }
    
}