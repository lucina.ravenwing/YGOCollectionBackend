using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using YGOCollectionBackend.Controllers;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend_UnitTests.ControllerUnitTests;

public class DeckControllerUnitTests {
    [Fact]
    public async void GetDecksWithNullNameClaimsReturnsUnauthorized() {
        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "mock"));

        DeckController deckController = new(new Mock<IDeckService>().Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.GetDecks();
        Assert.IsType<UnauthorizedObjectResult>(response);

        UnauthorizedObjectResult? responseConvert = response as UnauthorizedObjectResult;
        Assert.Contains("Error", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void GetDecksWithProperInfoReturnsOkResponse() {
        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "mock"));

        Mock<IDeckService> mockDeckService = new();
        mockDeckService.Setup(service => service.GetDecksAsync(It.IsAny<ClaimsIdentity>()))
            .ReturnsAsync(new List<DeckDataVm>());

        DeckController deckController = new(mockDeckService.Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.GetDecks();
        Assert.IsType<OkObjectResult>(response);

        OkObjectResult? responseConvert = response as OkObjectResult;
        Assert.IsType<List<DeckDataVm>>(responseConvert?.Value);
    }

    [Fact]
    public async void SaveDeckWithNoClaimsReturnsUnauthorized() {
        SaveDeckContentsVm[] deckContents = {
            new()
        };

        SaveDeckVm deck = new() {
            DeckName = "testDeck",
            DeckContents = deckContents
        };

        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "mock"));

        DeckController deckController = new(new Mock<IDeckService>().Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.SaveDeck(deck);
        Assert.IsType<UnauthorizedObjectResult>(response);

        UnauthorizedObjectResult? responseConvert = response as UnauthorizedObjectResult;
        Assert.Contains("Error", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void SaveDeckWithNullDeckContentsReturnsBadRequest() {
        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "mock"));

        DeckController deckController = new(new Mock<IDeckService>().Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.SaveDeck(new SaveDeckVm());
        Assert.IsType<BadRequestObjectResult>(response);

        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;
        Assert.Contains("Error: Deck contents are null.", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void SaveDeckWithTooManyDeckContentsReturnsBadRequest() {
        SaveDeckContentsVm[] deckContents = new SaveDeckContentsVm[91];

        for (int i = 0; i < 91; i++) {
            deckContents[i] = new SaveDeckContentsVm();
        }

        SaveDeckVm deck = new() {
            DeckName = "testDeck",
            DeckContents = deckContents
        };

        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "mock"));

        DeckController deckController = new(new Mock<IDeckService>().Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.SaveDeck(deck);
        Assert.IsType<BadRequestObjectResult>(response);

        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;
        Assert.Contains("Error: Deck contents contain", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void SaveDeckCorrectInformationReturnsProperData() {
        SaveDeckContentsVm[] deckContents = {
            new()
        };

        SaveDeckVm deck = new() {
            DeckName = "testDeck",
            DeckContents = deckContents
        };

        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "mock"));

        Mock<IDeckService> mockDeckService = new();
        mockDeckService.Setup(service => service.SaveDeckAsync(It.IsAny<SaveDeckVm>(), It.IsAny<ClaimsIdentity>()))
            .ReturnsAsync(new Tuple<int, string>(200, "Success."));

        DeckController deckController = new(mockDeckService.Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.SaveDeck(deck);
        Assert.IsType<ObjectResult>(response);

        ObjectResult? responseConvert = response as ObjectResult;
        Assert.Equal(200, responseConvert?.StatusCode);
    }

    [Fact]
    public async void UpdateDeckWithNoClaimsReturnsUnauthorized() {
        UpdateDeckContentsVm[] deckContents = {
            new()
        };

        UpdateDeckVm deck = new() {
            DeckName = "testDeck",
            DeckContents = deckContents
        };

        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "mock"));

        DeckController deckController = new(new Mock<IDeckService>().Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.UpdateDeck(deck);
        Assert.IsType<UnauthorizedObjectResult>(response);

        UnauthorizedObjectResult? responseConvert = response as UnauthorizedObjectResult;
        Assert.Contains("Error", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void UpdateDeckWithNullDeckContentsReturnsBadRequest() {
        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "mock"));

        DeckController deckController = new(new Mock<IDeckService>().Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.UpdateDeck(new UpdateDeckVm());
        Assert.IsType<BadRequestObjectResult>(response);

        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;
        Assert.Contains("Error: Deck contents are null.", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void UpdateDeckWithTooManyDeckContentsReturnsBadRequest() {
        UpdateDeckContentsVm[] deckContents = new UpdateDeckContentsVm[91];

        for (int i = 0; i < 91; i++) {
            deckContents[i] = new UpdateDeckContentsVm();
        }

        UpdateDeckVm deck = new() {
            DeckName = "testDeck",
            DeckContents = deckContents
        };

        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "mock"));

        DeckController deckController = new(new Mock<IDeckService>().Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.UpdateDeck(deck);
        Assert.IsType<BadRequestObjectResult>(response);

        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;
        Assert.Contains("Error: Deck contents contain", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void UpdateDeckCorrectInformationReturnsProperData() {
        UpdateDeckContentsVm[] deckContents = {
            new()
        };
        
        UpdateDeckVm deck = new() {
            DeckName = "testDeck",
            DeckContents = deckContents
        };

        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "mock"));

        Mock<IDeckService> mockDeckService = new();
        mockDeckService.Setup(service => service.UpdateDeckAsync(It.IsAny<UpdateDeckVm>(), It.IsAny<ClaimsIdentity>()))
            .ReturnsAsync(new Tuple<int, string>(200, "Success."));

        DeckController deckController = new(mockDeckService.Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.UpdateDeck(deck);
        Assert.IsType<ObjectResult>(response);

        ObjectResult? responseConvert = response as ObjectResult;
        Assert.Equal(200, responseConvert?.StatusCode);
    }
    
        [Fact]
    public async void DeleteDeckWithNoClaimsReturnsUnauthorized() {
        DeleteDeckVm deck = new() {
            DeckId = 4
        };
        
        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "mock"));
        
        DeckController deckController = new(new Mock<IDeckService>().Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.DeleteDeck(deck);
        Assert.IsType<UnauthorizedObjectResult>(response);

        UnauthorizedObjectResult? responseConvert = response as UnauthorizedObjectResult;
        Assert.Contains("Error", responseConvert?.Value?.ToString());
    }
    
    [Fact]
    public async void DeleteDeckCorrectInformationReturnsProperData() {
        DeleteDeckVm deck = new() {
            DeckId = 4
        };
        
        ClaimsPrincipal user = new(new ClaimsIdentity(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "mock"));
        
        Mock<IDeckService> mockDeckService = new();
        mockDeckService.Setup(service => service.DeleteDeckAsync(It.IsAny<long>(), It.IsAny<ClaimsIdentity>())).ReturnsAsync(new Tuple<int, string>(200, "Success."));
        
        DeckController deckController = new(mockDeckService.Object);
        deckController.ControllerContext.HttpContext = new DefaultHttpContext() {
            User = user
        };

        IActionResult response = await deckController.DeleteDeck(deck);
        Assert.IsType<ObjectResult>(response);

        ObjectResult? responseConvert = response as ObjectResult;
        Assert.Equal(200, responseConvert?.StatusCode);
    }
}