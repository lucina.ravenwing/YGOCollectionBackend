using Moq;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Services.Implementations;

namespace YGOCollectionBackend_UnitTests.ServiceUnitTests;

public class CardServiceUnitTests {

    [Fact]
    public async void GetCardByIdAsyncReturnsCard() {
        Mock<ICardRepo> mockCardRepo = new();

        Card card = new() {
            Atk = 1234,
            Def = 4321,
            Attribute = "Dark",
            CardSets = new List<CardSet>(),
            CardText = "This is some card text.",
            CardType = "Monster",
            GoatLimitations = "",
            Id = 1234,
            ImageUrls = "",
            Level = 2,
            LinkMarkers = "",
            LinkValue = 0,
            MasterDuelRarity = "N",
            Name = "Test",
            OcgLimitations = "",
            Scale = 0,
            SortOrder = 1,
            TcgLimitations = "",
            TreatedAs = "",
            TypeCategory = "Warrior"
        };

        mockCardRepo.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(card);

        CardServiceImpl cardServiceImpl = new(mockCardRepo.Object);

        Card response = await cardServiceImpl.GetCardByIdAsync(1234);
        
        Assert.Equal("Test", response.Name);
    }
}