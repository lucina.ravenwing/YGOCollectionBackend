using Moq;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Services.Implementations;

namespace YGOCollectionBackend_UnitTests.ServiceUnitTests;

public class DeckContentServiceUnitTests {

    [Fact]
    public void GetDeckContentsByDeckIdReturnsListOfDeckContent() {
        Mock<IDeckContentRepo> mockDeckContentRepo = new();

        List<DeckContent> deckContents = new();
        
        deckContents.Add(new DeckContent() {
            CardSet = new CardSet(),
            CardSetCode = "AGOV",
            CardSetRarityCode = "SR",
            DeckId = 1,
            Position = 1
        });
        
        deckContents.Add(new DeckContent() {
            CardSet = new CardSet(),
            CardSetCode = "AGOV",
            CardSetRarityCode = "UR",
            DeckId = 1,
            Position = 2
        });
        
        mockDeckContentRepo.Setup(repo => repo.GetDeckContentsByDeckId(It.IsAny<long>())).Returns(deckContents);

        DeckContentServiceImpl deckContentServiceImpl = new(mockDeckContentRepo.Object);

        List<DeckContent> response = deckContentServiceImpl.GetDeckContentsByDeckId(1234);
        
        Assert.NotEmpty(response);
    }

    [Fact]
    public void SaveDeckContentAsyncRunsSuccessfully() {
        Mock<IDeckContentRepo> mockDeckContentRepo = new();

        mockDeckContentRepo.Setup(repo => repo.SaveDeckContentAsync(It.IsAny<IEnumerable<DeckContent>>()));
        
        DeckContentServiceImpl deckContentServiceImpl = new(mockDeckContentRepo.Object);

        deckContentServiceImpl.SaveDeckContentAsync(new List<DeckContent>());
    }

    [Fact]
    public void DeleteDeckContentsByDeckIdAsyncRunsSuccessfully() {
        Mock<IDeckContentRepo> mockDeckContentRepo = new();

        mockDeckContentRepo.Setup(repo => repo.DeleteDeckContentsByDeckIdAsync(It.IsAny<long>()));
        
        DeckContentServiceImpl deckContentServiceImpl = new(mockDeckContentRepo.Object);

        deckContentServiceImpl.DeleteDeckContentsByDeckIdAsync(1234);
    }
    
}