using Moq;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Services.Implementations;

namespace YGOCollectionBackend_UnitTests.ServiceUnitTests;

public class CardSetServiceUnitTests {

    [Fact]
    public async void GetCardSetByCodeAndRarityCodeAsyncReturnsCardSet() {
        Mock<ICardSetRepo> mockCardSetRepo = new();

        CardSet cardSet = new() {
            Card = new Card(),
            CardId = 1234,
            CardSetCode = "AGOV",
            CardSetPrice = 12.34f,
            CardSetRarity = "Super Rare",
            CardSetRarityCode = "SR",
            SetName = "Age of Overlord"
        };

        mockCardSetRepo.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync
            (It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(cardSet);

        CardSetServiceImpl cardSetServiceImpl = new(mockCardSetRepo.Object);

        CardSet response = await cardSetServiceImpl.GetCardSetByCodeAndRarityCodeAsync("AGOV", "SR");

        Assert.Equal("AGOV", response.CardSetCode);
    }
    
    [Fact]
    public void GetCardSetByCardIdReturnsCardSet() {
        Mock<ICardSetRepo> mockCardSetRepo = new();

        CardSet cardSet = new() {
            Card = new Card(),
            CardId = 1234,
            CardSetCode = "AGOV",
            CardSetPrice = 12.34f,
            CardSetRarity = "Super Rare",
            CardSetRarityCode = "SR",
            SetName = "Age of Overlord"
        };
        CardSet cardSet2 = new() {
            Card = new Card(),
            CardId = 1234,
            CardSetCode = "AGOV",
            CardSetPrice = 12.34f,
            CardSetRarity = "Super Rare",
            CardSetRarityCode = "SR",
            SetName = "Age of Overlord"
        };

        IEnumerable<CardSet> cardSets = new List<CardSet>() {
            cardSet,
            cardSet2
        };

        mockCardSetRepo.Setup(repo => repo.GetCardSetsByCardId(It.IsAny<int>())).Returns(cardSets.AsQueryable());

        CardSetServiceImpl cardSetServiceImpl = new(mockCardSetRepo.Object);

        List<CardSet> response = cardSetServiceImpl.GetCardSetsByCardId(1234);

        Assert.NotEmpty(response);
    }
}