using Moq;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Services.Implementations;

namespace YGOCollectionBackend_UnitTests.ServiceUnitTests;

public class UserServiceUnitTests {

    [Fact]
    public async void GetUserByUsernameWithExistingUserReturnsUser() {
        Mock<IUserRepo> mockUserRepo = new();

        mockUserRepo.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Username = "testUser",
            Email = "test@test.com",
            Id = 343434343
        });
        
        UserServiceImpl userServiceImpl = new(mockUserRepo.Object);

        User response = await userServiceImpl.GetUserByUsernameAsync("testUser");

        Assert.Equal("testUser", response.Username);
    }
    
    [Fact]
    public async void GetUserByEmailWithExistingUserReturnsUser() {
        Mock<IUserRepo> mockUserRepo = new();

        mockUserRepo.Setup(repo => repo.GetUserByEmailAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Username = "testUser",
            Email = "test@test.com",
            Id = 343434343
        });
        
        UserServiceImpl userServiceImpl = new(mockUserRepo.Object);

        User response = await userServiceImpl.GetUserByEmailAsync("test@test.com");

        Assert.Equal("test@test.com", response.Email);
    }
    
    [Fact]
    public async void CreateUserReturnsAUser() {
        Mock<IUserRepo> mockUserRepo = new();

        User user = new() {
            Username = "testUser",
            Email = "test@test.com",
            Id = 343434343
        };

        mockUserRepo.Setup(repo => repo.CreateUserAsync(It.IsAny<User>())).ReturnsAsync(user);
        
        UserServiceImpl userServiceImpl = new(mockUserRepo.Object);

        User response = await userServiceImpl.CreateUserAsync(user);

        Assert.Equal("testUser", response.Username);
    }
}