using System.Security.Claims;
using Moq;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Repos.Interfaces;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;
using YGOCollectionBackend.Services.Implementations;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend_UnitTests.ServiceUnitTests;

public class DeckServiceUnitTests {
    [Fact]
    public async void GetDecksAsyncWithNoUserReturnsBlankList() {
        Mock<IDeckRepo> mockDeckService = new();
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        List<DeckDataVm> response = await service.GetDecksAsync(user);

        Assert.Empty(response);
    }

    [Fact]
    public async void GetDecksAsyncReturnsAListOfDeckDataVms() {
        Deck deck = new() {
            DeckName = "testDeck",
            Id = 4,
            UserId = 4
        };

        Deck deck2 = new() {
            DeckName = "testDeck2",
            Id = 5,
            UserId = 4
        };

        DeckContent deckContent = new() {
            CardSetCode = "BLMR-0063",
            CardSetRarityCode = "UR",
            DeckId = 4,
            Position = 1,
            CardSet = new CardSet {
                Card = new Card()
            }
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDecksByUserId(It.IsAny<long>())).Returns(new List<Deck> { deck, deck2 });

        Mock<IDeckContentService> mockDeckContentService = new();
        mockDeckContentService.Setup(repo => repo.GetDeckContentsByDeckId(It.IsAny<long>()))
            .Returns(new List<DeckContent> { deckContent });

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Id = 4,
            Email = "lucinaravenwing.net@gmail.com",
            Username = "auraravenwing"
        });

        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        List<DeckDataVm> response = await service.GetDecksAsync(user);

        Assert.Equal(2, response.Count);
    }

    [Fact]
    public async void SaveDeckAsyncWithNoAppUserReturns500Error() {
        Mock<IDeckRepo> mockDeckService = new();
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(new SaveDeckVm(), user);

        Assert.Equal(500, response.Item1);
        Assert.Contains("Error: User not able to be found", response.Item2);
    }

    [Fact]
    public async void SaveDeckAsyncWithAlreadyExistingDeckReturns400Error() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(true);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(new SaveDeckVm(), user);

        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: Deck already exists.", response.Item2);
    }
    
    [Fact]
    public async void SaveDeckWithInvalidSubdeckValueReturnsError() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card());


        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = new[] {
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 70,
                    Subdeck = 'B'
                }
            }
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: Not a valid sub deck value.", response.Item2);
    }

    [Fact]
    public async void SaveDeckWithTooManyCardsInMainDeckReturnsError() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = "Effect"
        });


        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = new[] {
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                }
            }
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: Too many cards in the main deck.", response.Item2);
    }

    [Fact]
    public async void SaveDeckWithTooManyCardsInExtraDeckReturnsError() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = "Fusion"
        });


        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = new[] {
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                }
            }
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: Too many cards in the extra deck.", response.Item2);
    }

    [Fact]
    public async void SaveDeckWithTooManyCardsInSideDeckReturnsError() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = "Effect"
        });


        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = new[] {
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'S'
                }
            }
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: Too many cards in the side deck.", response.Item2);
    }

    [Fact]
    public async void SaveDeckWithExtraDeckCardInMainDeckReturnsError() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = "Fusion"
        });


        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = new[] {
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                }
            }
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: There is an extra deck monster in the main or side deck.", response.Item2);
    }

    [Fact]
    public async void SaveDeckWithMainDeckCardInExtraDeckReturnsError() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = "Effect"
        });


        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = new[] {
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                }
            }
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: There is a non-extra deck card in the extra deck.", response.Item2);
    }

    [Fact]
    public async void SaveDeckWithNullCardTypeReturnsError() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = null
        });


        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = new[] {
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'E'
                }
            }
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: Card type is null.", response.Item2);
    }

    [Fact]
    public async void SaveDeckAsyncWithSavedDeckIdOf0Returns500Error() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(new Deck() {
            Id = 0
        });

        Mock<IDeckContentService> mockDeckContentService = new();

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(new SaveDeckVm(), user);

        Assert.Equal(500, response.Item1);
        Assert.Contains("Error: Deck not saved for unknown reason.", response.Item2);
    }

    [Fact]
    public async void SaveDeckAsyncWithNullDeckContentsReturns200() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card());


        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = null
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(200, response.Item1);
        Assert.Contains("Deck saved successfully.", response.Item2);
    }

    [Fact]
    public async void SaveDeckAsyncWithProperDataReturns200() {
        User appUser = new() {
            Username = "testuser",
            Email = "testemail@test.com",
            Id = 4
        };

        Deck deck = new() {
            Id = 4,
            UserId = 4,
            DeckName = "testDeck"
        };

        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.ExistsByUserIdAndName(It.IsAny<long>(), It.IsAny<string>()))
            .Returns(false);
        mockDeckService.Setup(repo => repo.SaveDeckAsync(It.IsAny<Deck>())).ReturnsAsync(deck);

        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(appUser);

        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(new CardSet() {
                CardId = 1234,
                CardSetCode = "BLMR-EN062",
                CardSetRarity = "Ultra Rare",
                CardSetRarityCode = "UR"
            });

        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = "Effect"
        });

        SaveDeckVm saveDeckVm = new() {
            DeckName = "testDeck",
            DeckContents = new[] {
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 70,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 60,
                    Subdeck = 'M'
                },
                new SaveDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    Position = 91,
                    Subdeck = 'S'
                }
            }
        };

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl deckService = new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object,
            mockCardSetService.Object, mockCardService.Object);

        Tuple<int, string> response = await deckService.SaveDeckAsync(saveDeckVm, user);

        Assert.Equal(200, response.Item1);
        Assert.Contains("Deck saved successfully.", response.Item2);
    }

    [Fact]
    public async void UpdateDeckAsyncWithInvalidAppUserReturns500Error() {
        Mock<IDeckRepo> mockDeckService = new();
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.UpdateDeckAsync(new UpdateDeckVm(), user);
        
        Assert.Equal(500, response.Item1);
        Assert.Contains("Error: User not able to be found", response.Item2);
    }

    [Fact]
    public async void UpdateDeckAsyncWithNoSavedDeckReturns400Error() {
        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDeckByDeckIdAsync(It.IsAny<long>())).ReturnsAsync(new Deck());
        
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Id = 4
        });
        
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.UpdateDeckAsync(new UpdateDeckVm(), user);
        
        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: Deck does not exist", response.Item2);
    }
    
    [Fact]
    public async void UpdateDeckAsyncWithOwnedUserIdNotMatchingCallingUserIdReturns401Error() {
        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDeckByDeckIdAsync(It.IsAny<long>())).ReturnsAsync(new Deck() {
            UserId = 2
        });
        
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Id = 4
        });
        
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.UpdateDeckAsync(new UpdateDeckVm(), user);
        
        Assert.Equal(403, response.Item1);
        Assert.Contains("Error: Deck does not belong to given", response.Item2);
    }
    
    [Fact]
    public async void UpdateDeckAsyncWithNullDeckContentsReturns200() {
        UpdateDeckVm updateDeckVm = new() {
            DeckContents = null,
            DeckId = 3,
            DeckName = "testDeck"
        };
        
        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDeckByDeckIdAsync(It.IsAny<long>())).ReturnsAsync(new Deck() {
            UserId = 4
        });
        
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Id = 4
        });
        
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.UpdateDeckAsync(updateDeckVm, user);
        
        Assert.Equal(200, response.Item1);
        Assert.Contains("Deck updated successfully.", response.Item2);
    }
    
    [Fact]
    public async void UpdateDeckAsyncWithIncorrectSubDeckReturnsError() {
        UpdateDeckVm updateDeckVm = new() {
            DeckId = 3,
            DeckName = "testDeck",
            DeckContents = new[] {
                new UpdateDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    DeckId = 4,
                    Position = 4,
                    Subdeck = 'B'
                }
            }
        };
        
        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDeckByDeckIdAsync(It.IsAny<long>())).ReturnsAsync(new Deck() {
            UserId = 4
        });
        
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Id = 4
        });
        
        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(),
            It.IsAny<string>())).ReturnsAsync(new CardSet());
        
        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card());

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.UpdateDeckAsync(updateDeckVm, user);
        
        Assert.Equal(400, response.Item1);
        Assert.Contains("Error: Not a valid sub deck value.", response.Item2);
    }
    
    [Fact]
    public async void UpdateDeckAsyncWithCorrectDataReturns500() {
        UpdateDeckVm updateDeckVm = new() {
            DeckId = 3,
            DeckName = "testDeck",
            DeckContents = new[] {
                new UpdateDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    DeckId = 4,
                    Position = 4,
                    Subdeck = 'M'
                }
            }
        };
        
        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDeckByDeckIdAsync(It.IsAny<long>())).ReturnsAsync(new Deck() {
            UserId = 4
        });
        mockDeckService.Setup(repo => repo.UpdateDeck(It.IsAny<Deck>())).ReturnsAsync(new Deck());
        
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Id = 4
        });
        
        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(),
            It.IsAny<string>())).ReturnsAsync(new CardSet());
        
        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = "Effect"
        });

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.UpdateDeckAsync(updateDeckVm, user);
        
        Assert.Equal(500, response.Item1);
        Assert.Contains("Error: Deck not updated.", response.Item2);
    }
    
    [Fact]
    public async void UpdateDeckAsyncWithCorrectDataReturns200() {
        UpdateDeckVm updateDeckVm = new() {
            DeckId = 3,
            DeckName = "testDeck",
            DeckContents = new[] {
                new UpdateDeckContentsVm() {
                    CardSetCode = "BLMR-EN062",
                    CardSetRarityCode = "UR",
                    DeckId = 4,
                    Position = 4,
                    Subdeck = 'M'
                }
            }
        };
        
        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDeckByDeckIdAsync(It.IsAny<long>())).ReturnsAsync(new Deck() {
            UserId = 4
        });
        mockDeckService.Setup(repo => repo.UpdateDeck(It.IsAny<Deck>())).ReturnsAsync(new Deck() {
            UserId = 4
        });
        
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User {
            Id = 4
        });
        
        Mock<ICardSetService> mockCardSetService = new();
        mockCardSetService.Setup(repo => repo.GetCardSetByCodeAndRarityCodeAsync(It.IsAny<string>(),
            It.IsAny<string>())).ReturnsAsync(new CardSet());
        
        Mock<ICardService> mockCardService = new();
        mockCardService.Setup(repo => repo.GetCardByIdAsync(It.IsAny<long>())).ReturnsAsync(new Card() {
            CardType = "Effect"
        });

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.UpdateDeckAsync(updateDeckVm, user);
        
        Assert.Equal(200, response.Item1);
        Assert.Contains("Deck updated successfully.", response.Item2);
    }

    [Fact]
    public async void DeleteDeckWithUnknownUserReturns500() {
        Mock<IDeckRepo> mockDeckService = new();
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.NameIdentifier, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.DeleteDeckAsync(2, user);
        
        Assert.Equal(500, response.Item1);
        Assert.Contains("Error: User not able to be found", response.Item2);
    }

    [Fact]
    public async void DeleteDeckWithUserIdAndDeckUserIdNotMatchingReturns401() {
        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDeckByDeckIdAsync(It.IsAny<long>())).ReturnsAsync(new Deck() {
            UserId = 3
        });
        
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User() {
            Id = 4
        });
        
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.DeleteDeckAsync(2, user);
        
        Assert.Equal(403, response.Item1);
        Assert.Contains("Error: Given user doesn't own the provided deck.", response.Item2);
    }

    [Fact]
    public async void DeleteDeckWithProperDataReturns200() {
        Mock<IDeckRepo> mockDeckService = new();
        mockDeckService.Setup(repo => repo.GetDeckByDeckIdAsync(It.IsAny<long>())).ReturnsAsync(new Deck() {
            UserId = 4
        });
        
        Mock<IDeckContentService> mockDeckContentService = new();
        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(repo => repo.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(new User() {
            Id = 4
        });
        
        Mock<ICardSetService> mockCardSetService = new();
        Mock<ICardService> mockCardService = new();

        ClaimsIdentity user = new(new Claim[] {
            new(ClaimTypes.Name, "auraravenwing")
        }, "user");

        DeckServiceImpl service =
            new(mockDeckService.Object, mockDeckContentService.Object, mockUserService.Object, mockCardSetService.Object,
                mockCardService.Object);

        Tuple<int, string> response = await service.DeleteDeckAsync(2, user);
        
        Assert.Equal(200, response.Item1);
        Assert.Contains("Deck deleted successfully.", response.Item2);
    }
}