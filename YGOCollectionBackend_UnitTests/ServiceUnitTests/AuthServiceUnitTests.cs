using System.Net;
using Microsoft.Extensions.Configuration;
using Moq;
using YGOCollectionBackend.Data.Models;
using YGOCollectionBackend.Data.Requests.ViewModel;
using YGOCollectionBackend.Data.Responses;
using YGOCollectionBackend.HttpWrapper;
using YGOCollectionBackend.Services.Implementations;
using YGOCollectionBackend.Services.Interfaces;

namespace YGOCollectionBackend_UnitTests.ServiceUnitTests;

public class AuthServiceUnitTests {
    private static Mock<IConfiguration> SetUpMockConfiguration() {
        Mock<IConfigurationSection> mockSection = new();
        mockSection.Setup(section => section.Value).Returns("this-is-a-key");

        Mock<IConfiguration> mockConfiguration = new();
        mockConfiguration.Setup(config => config.GetSection(It.IsAny<string>())).Returns(mockSection.Object);

        return mockConfiguration;
    }

    [Fact]
    public async void SignUpAsyncReturnsProperMessage() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("\"User created successfully!\"");

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        Mock<IUserService> mockUserService = new();

        User user = new() {
            Username = "testUser"
        };

        mockUserService.Setup(userService => userService.CreateUserAsync(It.IsAny<User>())).ReturnsAsync(user);

        AuthServiceImpl authService = new(mockClient.Object, mockUserService.Object, mockConfig.Object);

        Tuple<int, string> response = await authService.SignUpAsync(new SignUpRequestVm());

        Assert.Equal(200, response.Item1);
        Assert.Equal("User created successfully!", response.Item2);
    }

    [Fact]
    public async void SignUpAsyncReturnsProperMessageAndHitsUserNotCreated() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("\"User created successfully!\"");

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        Mock<IUserService> mockUserService = new();

        User user = new();

        mockUserService.Setup(userService => userService.CreateUserAsync(It.IsAny<User>())).ReturnsAsync(user);

        AuthServiceImpl authService = new(mockClient.Object, mockUserService.Object, mockConfig.Object);

        Tuple<int, string> response = await authService.SignUpAsync(new SignUpRequestVm());

        Assert.Equal(200, response.Item1);
        Assert.Equal("User created successfully!", response.Item2);
    }

    [Fact]
    public async void SignUpAsyncWithErrorReturnsProperMessage() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.BadRequest);
        postResponse.Content = new StringContent("\"User already exists.");

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        Mock<IUserService> mockUserService = new();

        AuthServiceImpl authService = new(mockClient.Object, mockUserService.Object, mockConfig.Object);

        Tuple<int, string> response = await authService.SignUpAsync(new SignUpRequestVm());

        Assert.Equal(400, response.Item1);
        Assert.Equal("User already exists.", response.Item2);
    }

    [Fact]
    public async void LogInAsyncWith4XxErrorReturnsBlankToken() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.BadRequest);
        postResponse.Content = new StringContent("{ \"message\": \"Error: User doesn't exist.\" }");

        LogInRequestVm logInRequest = new() {
            Password = "Password",
            UserNameEmail = "Username"
        };

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        Mock<IUserService> mockUserService = new();

        AuthServiceImpl authService = new(mockClient.Object, mockUserService.Object, mockConfig.Object);
        
        Tuple<int, LogInResponse> response = await authService.LogInAsync(logInRequest);

        Assert.Equal("", response.Item2.Token);
    }

    [Fact]
    public async void LogInAsyncWithProperInputsWithUsernameReturnsToken() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("{ \"message\": \"Token created successfully!\", " +
                                                 "\"token\": \"this-is-a-token\", \"refreshToken\": \"this-is-a-refresh-token\", " +
                                                 "\"expiresAt\": \"2023-05-03\" }");

        LogInRequestVm logInRequest = new() {
            Password = "Password",
            UserNameEmail = "Username"
        };

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(userService => userService.GetUserByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(new User() { Username = "Exists" });

        AuthServiceImpl authService = new(mockClient.Object, mockUserService.Object, mockConfig.Object);

        Tuple<int, LogInResponse> response = await authService.LogInAsync(logInRequest);

        Assert.Equal("Token created successfully!", response.Item2.Message);
        Assert.Equal("this-is-a-token", response.Item2.Token);
        Assert.Equal("this-is-a-refresh-token", response.Item2.RefreshToken);
        Assert.Equal("2023-05-03", response.Item2.ExpiresAt);
    }

    [Fact]
    public async void LogInAsyncWithProperInputsWithEmailReturnsToken() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("{ \"message\": \"Token created successfully!\", " +
                                                 "\"token\": \"this-is-a-token\", \"refreshToken\": \"this-is-a-refresh-token\", " +
                                                 "\"expiresAt\": \"2023-05-03\" }");

        LogInRequestVm logInRequest = new() {
            Password = "Password",
            UserNameEmail = "Username@username.com"
        };

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(userService => userService.GetUserByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new User() { Username = "Exists" });

        AuthServiceImpl authService = new(mockClient.Object, mockUserService.Object, mockConfig.Object);

        Tuple<int, LogInResponse> response = await authService.LogInAsync(logInRequest);

        Assert.Equal("Token created successfully!", response.Item2.Message);
        Assert.Equal("this-is-a-token", response.Item2.Token);
        Assert.Equal("this-is-a-refresh-token", response.Item2.RefreshToken);
        Assert.Equal("2023-05-03", response.Item2.ExpiresAt);
    }

    [Fact]
    public async void LogInWithUserThatDoesNotExistInCurrentDatabaseAndGoodResponseFromServerRunsFine() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("{ \"message\": \"Token created successfully!\", " +
                                                 "\"token\": \"this-is-a-token\", \"refreshToken\": \"this-is-a-refresh-token\", " +
                                                 "\"expiresAt\": \"2023-05-03\" }");

        HttpResponseMessage getUserDataResponse = new(HttpStatusCode.OK);
        getUserDataResponse.Content = new StringContent("{" +
                                                        "\"username\": \"username\"," +
                                                        "\"email\": \"email\"," +
                                                        "\"message\": \"message\""+
                                                        "}");

        LogInRequestVm logInRequest = new() {
            Password = "Password",
            UserNameEmail = "Username@username.com"
        };

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync("https://auth.lucinaravenwing.net/api/auth/log-in", It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);
        mockClient.Setup(client => client.PostAsync("https://auth.lucinaravenwing.net/api/user/get-user-data", It.IsAny<StringContent>()))
            .ReturnsAsync(getUserDataResponse);

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(userService => userService.GetUserByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new User());
        mockUserService.Setup(repo => repo.CreateUserAsync(It.IsAny<User>()))
            .ReturnsAsync(new User() { Username = "Exists", Email = "Exists" });

        AuthServiceImpl authService = new(mockClient.Object, mockUserService.Object, mockConfig.Object);

        Tuple<int, LogInResponse> response = await authService.LogInAsync(logInRequest);

        Assert.Equal("Token created successfully!", response.Item2.Message);
        Assert.Equal("this-is-a-token", response.Item2.Token);
        Assert.Equal("this-is-a-refresh-token", response.Item2.RefreshToken);
        Assert.Equal("2023-05-03", response.Item2.ExpiresAt);
    }

    [Fact]
    public async void LogOutWithImproperInputReturnsBadRequest() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();
    
        HttpResponseMessage postResponse = new(HttpStatusCode.BadRequest);
        postResponse.Content = new StringContent("User log out failed.");
    
        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);
    
        AuthServiceImpl authService = new(mockClient.Object, new Mock<IUserService>().Object, mockConfig.Object);
    
        Tuple<int, string> response = await authService.LogOutAsync("");
    
        Assert.Equal(400, response.Item1);
        Assert.Equal("User log out failed.", response.Item2);
    }

    [Fact]
    public async void LogOutWithProperInformationReturnsCorrectly() {
        Mock<IConfiguration> mockConfig = SetUpMockConfiguration();
    
        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("User logged out successfully.");
    
        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);
    
        AuthServiceImpl authService = new(mockClient.Object, new Mock<IUserService>().Object, mockConfig.Object);

        Tuple<int, string> response = await authService.LogOutAsync("");
    
        Assert.Equal(200, response.Item1);
        Assert.Equal("User logged out successfully.", response.Item2);
    }
    
    [Fact]
    public async void ResendConfirmationEmailAsyncReturnsProperInformation() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();
        
        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("{ \"Message\": \"Confirmation email sent successfully.\" }");
        
        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        AuthServiceImpl mockAuthService =
            new(mockClient.Object, new Mock<IUserService>().Object, mockConfiguration.Object);

        Tuple<int, string> response = await mockAuthService.ResendConfirmationEmailAsync("lucinaravenwing.net@gmail.com");
        
        Assert.Equal(200, response.Item1);
        Assert.Equal("{ \"Message\": \"Confirmation email sent successfully.\" }", response.Item2);
    }
    
    [Fact]
    public async void ForgotPasswordAsyncReturnsProperInformation() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();
        
        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("{ \"Message\": \"Reset password email sent successfully.\" }");
        
        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        AuthServiceImpl mockAuthService =
            new(mockClient.Object, new Mock<IUserService>().Object, mockConfiguration.Object);

        ForgotPasswordVm body = new() {
            UsernameEmail = "lucinaravenwing.net@gmail.com"
        };

        Tuple<int, string> response = await mockAuthService.ForgotPasswordAsync(body);
        
        Assert.Equal(200, response.Item1);
        Assert.Equal("{ \"Message\": \"Reset password email sent successfully.\" }", response.Item2);
    }

    [Fact]
    public async void ResetPasswordAsyncWith400ErrorReturnsProperInformation() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.BadRequest);
        postResponse.Content = new StringContent("{ \"Message\": \"Error: Password not reset.\" }");

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        AuthServiceImpl mockAuthService =
            new(mockClient.Object, new Mock<IUserService>().Object, mockConfiguration.Object);
        
        ResetPasswordVm resetPassword = new() {
            Token = "This-is-a-token",
            NewPassword = "DontUseThisPassword",
            Email = "lucinaravenwing.net@gmail.com",
            Origin = "https://ygo.lucinaravenwing.net"
        };

        Tuple<int, string> response = await mockAuthService.ResetPasswordAsync(resetPassword);

        Assert.Equal(400, response.Item1);
        Assert.Equal("{ \"Message\": \"Error: Password not reset.\" }", response.Item2);
    }

    [Fact]
    public async void ResetPasswordAsyncReturnsProperInformation() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();
        
        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("{ \"Message\": \"Password reset successfully.\" }");

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        AuthServiceImpl mockAuthService =
            new(mockClient.Object, new Mock<IUserService>().Object, mockConfiguration.Object);
        
        ResetPasswordVm resetPassword = new() {
            Token = "This-is-a-token",
            NewPassword = "DontUseThisPassword",
            Email = "lucinaravenwing.net@gmail.com",
            Origin = "https://ygo.lucinaravenwing.net"
        };

        Tuple<int, string> response = await mockAuthService.ResetPasswordAsync(resetPassword);

        Assert.Equal(200, response.Item1);
        Assert.Equal("{ \"Message\": \"Password reset successfully.\" }", response.Item2);
    }
    
    [Fact]
    public async void TokenRefreshAsyncWith400ErrorReturnsProperInformation() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        HttpResponseMessage postResponse = new(HttpStatusCode.BadRequest);
        postResponse.Content = new StringContent("{ \"message\": \"Error: Token not refreshed.\" }");

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        AuthServiceImpl mockAuthService =
            new(mockClient.Object, new Mock<IUserService>().Object, mockConfiguration.Object);
        
        TokenRefreshVm tokenRefresh = new() {
            Token = "This-is-a-token",
            RefreshToken = "Refresh-Token"
        };

        Tuple<int, LogInResponse> response = await mockAuthService.TokenRefreshAsync(tokenRefresh);

        Assert.Equal(400, response.Item1);
        Assert.Equal("Error: Token not refreshed.", response.Item2.Message);
    }

    [Fact]
    public async void TokenRefreshAsyncReturnsProperInformation() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();
        
        HttpResponseMessage postResponse = new(HttpStatusCode.OK);
        postResponse.Content = new StringContent("{ \"message\": \"Token refreshed successfully\", " +
            "\"token\": \"this-is-a-token\", \"refreshToken\": \"this-is-a-refresh-token\", " +
            "\"expiresAt\": \"2023-05-03\" }");

        Mock<IHttpClient> mockClient = new();
        mockClient.Setup(client => client.PostAsync(It.IsAny<string>(), It.IsAny<StringContent>()))
            .ReturnsAsync(postResponse);

        AuthServiceImpl mockAuthService =
            new(mockClient.Object, new Mock<IUserService>().Object, mockConfiguration.Object);
        
        TokenRefreshVm tokenRefresh = new() {
            Token = "This-is-a-token",
            RefreshToken = "Refresh-Token"
        };

        Tuple<int, LogInResponse> response = await mockAuthService.TokenRefreshAsync(tokenRefresh);

        Assert.Equal(200, response.Item1);
        Assert.Equal("Token refreshed successfully", response.Item2.Message);
    }
}